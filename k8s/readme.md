- ca-config.json ca 配置文件
- ca-csr.json 证书签名请求文件

- 生成 ca 证书和 private key

> cfssl gencert -initca ca-csr.json | cfssljson -bare ca

会生成 ca.csr (certificate signing request) ca-key.pem (private key) ca.pem (public key) 三个文件

- 需要 private key, ca 及配置复制到节点 /etc/kubernetes/cert/

### kubectl

- kubectl 是 kubernetes 集群的命令行管理工具
- 节点默认从 ~/.kube/config 文件读取 kube-apiserver 地址、证书、用户名等信息.
- 下载文件 https://dl.k8s.io/v1.10.4/kubernetes-client-linux-amd64.tar.gz

- admin 证书和 private key

  -　配置文件 admin-crs.json

  ```shell
  cfssl gencert \
      -ca=ca.pem -ca-key=ca-key.pem \
      -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin
  ```

  生成了 admin.pem admin-key.pem admin.csr

- 创建 kubeconfig

  kubectl config 命令，使用了 ca 和 admin 的 private key 和证书，另外要用到 api server 的地址，最后生成 kubectl.kubeconfig 文件

- 分发 kubeconfig

  kubectl.kubeconfig 复制到所有节点的~/.kube/config

### etcd

- basic

  - etcd 存储所有运行数据

- etcd 创建证书和 private key, 配置文件 etcd/etcd-csr.json
- 将生成的 etcd.pem 和 etcd-key.pem 分发到节点 /etc/etcd/cert/
- 创建 etcd 的 systemd unit 模板文件 etcd.service.template，参数: NODE_IP, NODE_NAME, ETCD_NODES
- 根据情况分发到节点 /etc/systemd/system/etcd.service
- 启动并检查状态

### flannel 网络

- 基础知识:

  - k8s 要求集群中的节点能通过 Pod 网段互联互通．(?)
  - flannel 使用 vxlan 为各个节点创建一个各异互通的 Pod 网络．
  - udp 8472
  - flannel 第一次启动时从 etcd 获取 Pod 网段信息，为本节点分配一个未使用的 /24 段地址，然后创建新的接口
  - flannel 将分配的 Pod 网段信息写到 /run/flannel/docker 文件，docker 后续使用该文件中的变量设置 docker0 网桥
  - 当前版本 v0.10.0

- 下载和分发 flanneld binary

- 创建 flannel cert 和 private key

  - 该证书只被 kubectl 当作 client 证书使用, host 为空

- 向 etcd 写入集群 Pod 网段信息
- 创建 systemd unit 服务文件并分发
- 检查各 flanneld 的网段信息
- 验证各节点可以通过 Pod 网段互通

### master 节点

- 下载　 server 节点 binary

  wget https://dl.k8s.io/v1.10.4/kubernetes-server-linux-amd64.tar.gz

- ha

  - haproxy, 源安装，给出的配置 chroot，不会走 journald
  - keepalived vrrp，需要用 sudo

  > haproxy -- haproxy.cfg
  > sudo keepalived -n -l -f (pwd)/keepalived-(uname -n).conf

### worker 节点

###　验证集群功能

### 清理集群
