#!/usr/bin/env python
# coding=utf-8

import os
import shutil
import subprocess
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

output_files = ['etcd.pem', 'etcd-key.pem', 'etcd.csr']
for fname in output_files:
    if os.path.exists(fname):
        logging.info('removing file %s', fname)
        os.remove(fname)

ca = os.path.join(os.getcwd(), '../02-ca/ca.pem')
ca_key = os.path.join(os.getcwd(), '../02-ca/ca-key.pem')
config = os.path.join(os.getcwd(), '../02-ca/ca-config.json')
csr = 'etcd-csr.json'

command = f'cfssl gencert -ca={ca} -ca-key={ca_key} -config={config} -profile=kubernetes {csr} | cfssljson -bare etcd'
logging.info('command: %s', command)


logging.info('executing ...')
subprocess.check_call(command, shell=True)