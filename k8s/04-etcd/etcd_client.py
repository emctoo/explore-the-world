#!/usr/bin/env python
# coding=utf-8

import os
import etcd
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def client():

    cert = 'etcd.pem', 'etcd-key.pem'
    ca_cert = os.path.join(os.getcwd(), '../02-ca/ca.pem')

    client = etcd.Client(host='192.168.1.42', port=2379, allow_reconnect=True, protocol='https', cert=cert, ca_cert=ca_cert)
    logging.info('cluster machines: %s', client.machines)


client()
