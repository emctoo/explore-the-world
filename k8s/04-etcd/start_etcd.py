#!/usr/bin/env python
# coding=utf-8

import os
import platform
import subprocess
import logging

logger = logging.getLogger(__name__)

etcd_nodes = {
  'berry': '192.168.1.42',
  'compaq': '192.168.1.43',
  'acer': '192.168.1.44',
}

NODE_NAME, NODE_IP = platform.node(), etcd_nodes.get(platform.node())
ETCD_NODES = ','.join([f'{key}=https://{value}:2380' for key, value in etcd_nodes.items()])

data_dir = '/tmp/k8s/etcd'
if not os.path.exists(data_dir):
  os.makedirs(data_dir, exist_ok=True)

current_dir = os.getcwd()

cert_file = os.path.join(current_dir, 'etcd.pem')
key_file = os.path.join(current_dir, 'etcd-key.pem')

peer_cert_file = os.path.join(current_dir, 'etcd.pem')
peer_cert_key_file = os.path.join(current_dir, 'etcd-key.pem')

trusted_ca_file = os.path.join(current_dir, '../02-ca/ca.pem')
peer_trusted_ca_file = os.path.join(current_dir, '../02-ca/ca.pem')

commands = [
  f'./etcd-v3.3.10-linux-amd64/etcd',
  f'--data-dir={data_dir}',
  f'--name={NODE_NAME}',

  f'--cert-file={cert_file}',
  f'--key-file={key_file}',

  f'--peer-cert-file={peer_cert_file}',
  f'--peer-key-file={peer_cert_key_file}',

  f'--trusted-ca-file={trusted_ca_file}',
  f'--peer-trusted-ca-file={peer_trusted_ca_file}',

  f'--peer-client-cert-auth',
  f'--client-cert-auth',
  f'--listen-peer-urls=https://{NODE_IP}:2380',
  f'--initial-advertise-peer-urls=https://{NODE_IP}:2380',
  f'--listen-client-urls=https://{NODE_IP}:2379,http://127.0.0.1:2379',
  f'--advertise-client-urls=https://{NODE_IP}:2379',
  f'--initial-cluster-token=etcd-cluster-0',
  f'--initial-cluster={ETCD_NODES}',
  f'--initial-cluster-state=new',
]

try:
  subprocess.run(commands)
except KeyboardInterrupt:
  print('\rbye.')
