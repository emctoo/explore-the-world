# openssl 生成证书相关

### 基础知识

- [TODO] 这部分的知识需要进一步补充

### operations

- 生成 private key

> openssl genrsa -out server.key 1024

- 用该 private key 生成 public key

> openssl rsa -in server.key -pubout -out server.pem

- generate ca private key

> openssl genrsa -out ca.key 1024

- X.509 Certificate Signing Request (CSR) management

> openssl req -new -key ca.key -out ca.csr

- X.509 Certificate Data Management

> openssl x509 -req -in ca.csr -signkey ca.key -out ca.crt
