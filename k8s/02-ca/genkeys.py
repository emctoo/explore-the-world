#!/usr/bin/env python
# coding=utf-8

import os
import shutil
import subprocess
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

output_files = ['ca.pem', 'ca-key.pem', 'ca.csr']
for fname in output_files:
    if os.path.exists(fname):
        logging.info('removing file %s', fname)
        os.remove(fname)

logging.info('executing ...')
subprocess.check_call('cfssl gencert -initca ca-csr.json | cfssljson -bare ca', shell=True)