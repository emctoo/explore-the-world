#!/usr/bin/env python
# coding=utf-8

import os
import logging
import subprocess

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

ca = os.path.join(os.getcwd(), '../02-ca/ca.pem')
ca_key = os.path.join(os.getcwd(), '../02-ca/ca-key.pem')
config = os.path.join(os.getcwd(), '../02-ca/ca-config.json')
csr = 'flanneld-csr.json'

output_files = ['flanneld.csr', 'flanneld.pem', 'flanneld-key.pem']
for fname in output_files:
    if os.path.exists(fname):
        logging.info('removing file %s', fname)
        os.remove(fname)

command = f'cfssl gencert -ca={ca} -ca-key={ca_key} -config={config} -profile=kubernetes {csr} | cfssljson -bare flanneld'
logging.info('command [%s]', command)

logging.info('execute command ...')
try:
    subprocess.check_call(command, shell=True)  # 提示no hosts 也不用管
except KeyboardInterrupt:
    print('\rbye.')
