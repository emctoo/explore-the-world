#!/usr/bin/env python
# coding=utf-8

import sys
import etcd
import subprocess
import os
import logging
import platform

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

# Pod 网段，建议 /16 段地址，部署前路由不可达，部署后集群内路由可达(flanneld 保证)
# 必须和kube-controller-manager的 --cluster-cidr 一致
CLUSTER_CIDR = "172.30.0.0/16"

# 只能用v2的版本
'''
../04-etcd/etcd-v3.3.10-linux-amd64/etcdctl \
    --endpoints=https://192.168.1.42:2379,https://192.168.1.43:2379,https://192.168.1.44:2379 \
    --ca-file=../02-ca/ca.pem \
    --cert-file=flanneld.pem \
    --key-file=flanneld-key.pem \
    set /kubernetes/network/config '{"Network":"172.30.0.0/16", "SubnetLen": 24, "Backend": {"Type": "vxlan"}}'
'''


def client(host):
    name = 'flanneld'
    cert = f'{name}.pem', f'{name}-key.pem'
    ca_cert = os.path.join(os.getcwd(), '../02-ca/ca.pem')

    client = etcd.Client(host=host, port=2379, protocol='https', cert=cert, ca_cert=ca_cert)
    # logging.info('cluster machines: %s', client.machines)

    key = '/kubernetes/network/config'
    client.delete(key)

    value = {
        "Network": CLUSTER_CIDR,
        "SubnetLen": 24,
        "Backend": {
            "Type": "vxlan"
        },
    }

    result = client.write(key, value)
    logging.info('writing networking config: %s', result)


if __name__ == "__main__":
    host = '192.168.1.42' if len(sys.argv) == 1 else sys.argv[1]
    client(host)