#!/usr/bin/env python
# coding=utf-8

import os
import platform
import subprocess
import logging
import etcd

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

etcd_nodes = {
    'berry': '192.168.1.42',
    'compaq': '192.168.1.43',
    'acer': '192.168.1.44',
}

node_ifaces = {
    'berry': 'wlp2s0',
    'compaq': 'wlp0s26u1u2',
    'acer': 'wlp3s0',
}

etcd_endpoints = [
    'https://192.168.1.42:2379',
    'https://192.168.1.43:2379',
    'https://192.168.1.44:2379',
]


def list_all(host='192.168.1.42', name='flanneld'):
    cert = f'{name}.pem', f'{name}-key.pem'
    ca_cert = os.path.join(os.getcwd(), '../02-ca/ca.pem')
    client = etcd.Client(host=host, port=2379, protocol='https', cert=cert, ca_cert=ca_cert)
    logging.info('network config: [%s]', client.get('/kubernetes/network/config'))


list_all()

ETCD_ENDPOINTS_STR = ','.join(etcd_endpoints)
FLANNEL_ETCD_PREFIX = '/kubernetes/network'
IFACE = node_ifaces.get(platform.node())

logging.info('iface: %s, etcd_prefix: [%s]', IFACE, FLANNEL_ETCD_PREFIX)

etcd_cafile = os.path.join(os.getcwd(), '../02-ca/ca.pem')
etcd_certfile = 'flanneld.pem'
etcd_keyfile = 'flanneld-key.pem'

flanneld_command = [
    f'./flannel-v0.10.0-linux-amd64/flanneld',
    f'-etcd-cafile={etcd_cafile}',
    f'-etcd-certfile={etcd_certfile}',
    f'-etcd-keyfile={etcd_keyfile}',
    f'-etcd-endpoints={ETCD_ENDPOINTS_STR}',
    f'-etcd-prefix={FLANNEL_ETCD_PREFIX}',
    f'-iface={IFACE}',
]

# flanneld 使用系统缺省路由所在的接口与其它节点通信
# 对于有多个网络接口（如内网和公网）的节点，可以用 -iface 参数指定通信接口

try:
    subprocess.run(flanneld_command)
except KeyboardInterrupt:
    print('\rbye.')

# post_command = ['mk-docker-opts.sh', '-k', 'DOCKER_NETWORK_OPTIONS', '-d', '/run/flannel/docker']
