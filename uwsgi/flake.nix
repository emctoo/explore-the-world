{
  description = "Development environment for uWSGI with Python support in emperor mode";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        # Define Python environment first
        pythonEnv = pkgs.python3.withPackages (ps: with ps; [
          flask
        ]);

        # Custom uWSGI package with Python support
        customUwsgi = pkgs.uwsgi.override {
          plugins = [ "python3" ];
          python3 = pythonEnv;
        };

      in {
        packages = {
          uwsgi = customUwsgi;
          default = customUwsgi;
        };

        # Development shell with uWSGI and Python
        devShell = pkgs.mkShell {
          buildInputs = [
            customUwsgi
            pythonEnv
            pkgs.coreutils
          ];

          shellHook = ''
            export PYTHONPATH=${pythonEnv}/${pythonEnv.sitePackages}:$PYTHONPATH

            # Create directory structure if it doesn't exist
            mkdir -p vassals logs

            # Ensure the vassals config exists
            if [ ! -f vassals/flask.ini ]; then
              cp ${pkgs.writeText "flask.ini" (builtins.readFile ./vassals/flask.ini)} vassals/flask.ini
            fi

            echo "uWSGI emperor environment loaded"
            echo "Directory structure:"
            echo "  ./vassals/ - Contains vassal configurations"
            echo "  ./logs/    - Contains log files"
            echo ""
            echo "Available commands:"
            echo "  uwsgi --ini emperor.ini : Start the emperor"

            echo "PYTHONPATH: $PYTHONPATH"
            python3 -c "import flask; print(flask.__file__)"
          '';
        };
      });
}
