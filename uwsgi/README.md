## nixpkgs uwsgi
https://github.com/NixOS/nixpkgs/blob/7bdb5ae16e0ace4abd526a1d6f609f95f2dcaccb/pkgs/by-name/uw/uwsgi/package.nix
https://github.com/NixOS/nixpkgs/blob/49f1c6e940b67fa522c45c14603a0584aa90ebd2/nixos/modules/services/web-servers/uwsgi.nix

## app with uwsgi

uwsgi --ini app.ini

## vassals 目录结构

```text
├── app.py            # 你现有的 Flask 应用
├── emperor.ini       # Emperor 配置文件
├── flake.nix         # 更新后的 flake 配置
└── vassals/          # Vassal 配置目录
    └── flask.ini     # Flask 应用的 vassal 配置
```

```shell
uwsgi --ini emperor.ini
```

## die-on-term

`die-on-term = true` 是一个与进程终止信号处理相关的 uWSGI 配置选项。

### 作用

1. 当设置为 true 时：

  - 当 uWSGI 收到 SIGTERM 信号时，会立即终止所有进程
  - 不会等待正在处理的请求完成
  - 适用于需要快速关闭的场景，比如容器环境下的优雅关闭


2. 当设置为 false（默认值）时：

  - 收到 SIGTERM 信号后，会等待正在处理的请求完成
  - 会拒绝新的连接
  - 只有在所有请求处理完毕后才会关闭
  - 这种方式更"优雅"，但关闭时间可能会更长

### emperor 模式

- emperor 进程会向其管理的所有 vassal 发送相应的信号
- 如果设置了 `die-on-term = true`，当 emperor 收到 SIGTERM 时，会立即终止所有 vassal
- 如果没有设置，emperor 会等待 vassals 完成当前请求后再关闭

### 具体使用建议

- 开发环境：通常不需要设置，因为我们不太关心关闭时间
- 生产环境：如果在容器中运行，可能需要设置为 true 以符合容器的生命周期管理
- 如果你的应用处理长连接（如 WebSocket），可能需要设为 false 以确保连接能够正常关闭

### 测试

```bash
# 启动 uwsgi
uwsgi --ini emperor.ini

# 在另一个终端发送 SIGTERM
kill -TERM $(pgrep -f "uwsgi.*emperor")

# 观察日志中进程的关闭行为会有所不同。
```

## emperor tyrant

需要用 root 运行

https://github.com/unbit/uwsgi/issues/1776
