package main

import (
	"net/http"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	e.Static("/", "../web")

	api := e.Group("/api")
	api.GET("/hello", func(ctx echo.Context) error {
		return ctx.String(http.StatusOK, "world")
	})

	e.Logger.Fatal(e.Start(":8000"))
}
