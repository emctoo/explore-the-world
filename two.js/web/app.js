function createVertex(x, y) {
    let v = new Two.Vector(x, y);
    v.position = new Two.Vector().copy(v);
    return v;
}

function createGrid(size = 15) {
    console.log(`create grid ...`);

    let width = size, height = size, type = Two.Types.webgl;
    let two = new Two({ type, width, height });

    let a = two.makeLine(two.width / 2, 0, two.width / 2, two.height);
    let b = two.makeLine(0, two.height / 2, two.width, two.height / 2);
    a.stroke = '#6dcff6';
    b.stroke = '#6dcff6'

    two.update();

    // ?
    _.defer(function () {
        document.body.style.background = 'url(' + two.renderer.domElement.toDataURL('image/png') + ') 0 0 repeat';
        document.body.style.backgroundSize = `${width}px ${height}px`;
    });

}



let two = null; // new Two(twojs_options).appendTo(el);

let line = null;
let mouse = new Two.Vector();

let drag = function (e) {
    let x = e.clientX, y = e.clientY;

    if (line) {
        line.vertices.push(createVertex(x, y));
        mouse.set(x, y);
        return;
    }

    let v1 = createVertex(mouse.x, mouse.y), v2 = createVertex(x, y);
    line = two.makeCurve([v1, v2], true);
    line.noFill().stroke = '#33f';
    line.linewidth = 1.8;

    _.each(line.vertices, v => v.addSelf(line.translation));

    line.translation.clear();

    mouse.set(x, y);
};

let mouseUp = function (e) {
    window.removeEventListener('mousemove', drag);
    window.removeEventListener('mouseup', mouseUp);
};

let mouseDown = function (e) {
    mouse.set(e.clientX, e.clientY);
    line = null;

    window.addEventListener('mousemove', drag);
    window.addEventListener('mouseup', mouseUp);
};

let touchDrag = function (e) {
    e.preventDefault();
    let touch = e.changedTouches[0];
    drag({ clientX: touch.pageX, clientY: touch.pageY });
    return false;
};

let touchEnd = function (e) {
    e.preventDefault();
    window.removeEventListener('touchmove', touchDrag);
    window.removeEventListener('touchend', touchEnd);
    return false;
};

function touchStart(e) {
    e.preventDefault();

    let touch = e.changedTouches[0];
    mouse.set(touch.pageX, touch.pageY);
    line = null;

    window.addEventListener('touchmove', touchDrag);
    window.addEventListener('touchend', touchEnd);
    return false;
}

function updates(frameCount, timeDelta) {
    two.scene.children.forEach((child) => {
        child.vertices.forEach((v) => {
            if (v.position) v.x = v.position.x, v.y = v.position.y;
        });
    });
}

window.onload = () => {
    createGrid();

    // canvas, svg, webgl
    const twojs_options = { type: Two.Types['webgl'], fullscreen: true, autostart: true }
    const el = document.getElementById('drawing');
    two = new Two(twojs_options).appendTo(el);

    window.addEventListener('mousedown', mouseDown);
    window.addEventListener('touchstart', touchStart);

    two.bind('update', updates);
};
