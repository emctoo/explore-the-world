window.$vue = new Vue({
    el: '#drawing',
    data: {
        style: {
            width: '400px',
            height: '400px',
            border: 'solid red 1px',
        }
    },
    mounted() {
        console.log(`mounted`);
    },
    methods: {
        mouseDown(ev) {
            console.log(`mouse down`);
        },
        mouseUp(ev) {
            console.log(`mouse up`);
        }
    }
})