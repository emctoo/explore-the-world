#!/usr/bin/env python
# coding=utf-8

import sys, os
import subprocess
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

SOURCE = os.path.abspath(sys.argv[1])
DESTINATION = sys.argv[2]  # os.path.abspath(sys.argv[2])

# -m 一直监听, -r 递归, -q 打印, -e 事件
INOTIFYWAIT_COMMAND = ['inotifywait', '-m', '-r', '-q', '--timefmt', '%F.%T', '--format', '%T %w %f %e', '-e', 'modify,delete,create,attrib', SOURCE]
RSYNC_COMMAND = ['rsync', '-r', '--progress', '-a', '-h', '-q', '-z', '-t', '--delete', SOURCE, DESTINATION]


def list_files_recursively(directory, ignores=None):
    for dirpath, dirnames, filenames in os.walk(directory):
        logging.info('dirpath: %s, dirnames: %s, filenames: %s', dirpath, dirnames, filenames)


def main():
    # list_files_recursively(SOURCE)

    proc = subprocess.Popen(INOTIFYWAIT_COMMAND, stdout=subprocess.PIPE)

    fields = ['time', 'path', 'file', 'event']
    while True:
        values = proc.stdout.readline().decode().strip().split()
        result = dict(zip(fields, values))
        logging.info('output line: %s', result)

        logging.info('rsync ... [%s]', RSYNC_COMMAND)
        subprocess.run(RSYNC_COMMAND)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: %s <source> <destination>' % sys.argv[0])
        exit(0)

    logging.info('source: %s -> destination: %s', SOURCE, DESTINATION)

    try:
        main()
    except KeyboardInterrupt:
        print('\r')
