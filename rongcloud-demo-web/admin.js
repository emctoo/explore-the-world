function registerMessage(type, propertys) { // 注册自定义消息
	let messageName = type; // 消息名称。
	let objectName = "s:" + type; // 消息内置名称，请按照此格式命名 *:* 。
	let mesasgeTag = new RongIMLib.MessageTag(true, true); // true true 保存且计数，false false 不保存不计数。

	RongIMClient.registerMessageType(messageName, objectName, mesasgeTag, propertys);
}

function startInit($vue, appKey, token) {
	let params = { appKey, token };

	let callbacks = {
		getInstance: function (instance) {
			registerMessage("PersonMessage", ["name", "age", "gender"]); // 注册 PersonMessage
			registerMessage("ProductMessage", ["price", "title", "desc", "images"]); // 注册 ProductMessage
			$vue.instance = instance;
		},

		getCurrentUser: function (user) {
			console.dir(user);

			let chatRoomId = 'system';
			RongIMClient.getInstance().joinChatRoom(chatRoomId, 50, {
				onSuccess: () => console.log(`加入聊天室 ${chatRoomId} 成功`),
				onError: (error) => console.log(`加入聊天室 ${chatRoomId} 失败`, error)
			});
		},

		receiveNewMessage: function (message) { // 判断是否有 @ 自己的消息
			console.log(message);
			$vue.message.push(message)
		}
	};
	init(params, callbacks);
}

/*
message 数据结构
{
	"content": {
		"messageName": "TextMessage",
		"content": "阿拉部",
		"extra": {
			"name": "name",
			"age": 12
		},
		"user": {
			"userId": "this-is-a-test-id",
			"name": "张三",
			"portraitUri": "http://rongcloud.cn/images/newVersion/log_wx.png"
		}
	},
	"conversationType": 1,
	"objectName": "RC:TxtMsg",
	"messageDirection": 2,
	"messageId": "1_13887103",
	"receivedStatus": 0,
	"receivedTime": 1496373353260,
	"senderUserId": "user10",
	"sentTime": 1496287140858,
	"targetId": "user10",
	"messageType": "TextMessage",
	"messageUId": "5E63-AERV-843A-D3EE",
	"offLineMessage": true
}
*/

const columns = [
	{
		title: 'time',
		dataIndex: 'receivedTime',
		width: '80px',
	},
	{
		title: 'sender',
		dataIndex: 'senderUserId',
		width: '80px'
	},
	{
		title: 'receiver',
		dataIndex: 'targetId',
		width: '80px',
	}
	,
	{
		title: 'content',
		dataIndex: 'content.content'
	}
];

const $vue = new Vue({
	el: '#excel',
	data() {
		return {
			userId: 'user0x0000',
			instance: null,
			messages: [],
			columns,
		}
	},
	async mounted() {
		const { data } = await axios.post('/api/rc/token', { id: this.userId })
		startInit(this, data.appKey, data.token)
	},

	methods: {
	}
})