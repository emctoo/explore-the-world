/*
接收消息，并导出为文本

1. 接受存储新消息
	RongIMClient.setOnReceiveMessageListener({
		// 接收到的消息
		onReceived: function (message) {
		    messageOutput(message);
		}
	});
2. 控制台运行 messageOutput.show(); 获得blobUrl
3. 保存
4. message 数据结构
	{
		"content": {
			"messageName": "TextMessage",
			"content": "阿拉部",
			"extra": {
				"name": "name",
				"age": 12
			},
			"user": {
				"userId": "this-is-a-test-id",
				"name": "张三",
				"portraitUri": "http://rongcloud.cn/images/newVersion/log_wx.png"
			}
		},
		"conversationType": 1,
		"objectName": "RC:TxtMsg",
		"messageDirection": 2,
		"messageId": "1_13887103",
		"receivedStatus": 0,
		"receivedTime": 1496373353260,
		"senderUserId": "user10",
		"sentTime": 1496287140858,
		"targetId": "user10",
		"messageType": "TextMessage",
		"messageUId": "5E63-AERV-843A-D3EE",
		"offLineMessage": true
	}
*/

// 注册自定义消息
function registerMessage(type, propertys) {
	let messageName = type; // 消息名称。
	let objectName = "s:" + type; // 消息内置名称，请按照此格式命名 *:* 。
	let mesasgeTag = new RongIMLib.MessageTag(true, true); // true true 保存且计数，false false 不保存不计数。

	RongIMClient.registerMessageType(messageName, objectName, mesasgeTag, propertys);
}

function startInit($vue, appKey, token) {
	let params = { appKey, token };

	let callbacks = {
		getInstance: function (instance) {
			RongIMLib.RongIMEmoji.init();
			$vue.instance = $vue;

			registerMessage("PersonMessage", ["name", "age", "gender"]); // 注册 PersonMessage
			registerMessage("ProductMessage", ["price", "title", "desc", "images"]); // 注册 ProductMessage
		},

		getCurrentUser: function (userInfo) {
			console.dir(userInfo);
			let chatRoomId = 'system';
			RongIMClient.getInstance().joinChatRoom(chatRoomId, 50, {
				onSuccess: () => console.log(`加入聊天室 ${chatRoomId} 成功`),
				onError: (error) => console.log(`加入聊天室 ${chatRoomId} 失败`, error)
			});
		},

		receiveNewMessage: function (message) { // 判断是否有 @ 自己的消息
			console.log(message);
			$vue.messages.push(message);
			// if (message.content.mentionedInfo.userIdList.includes(userId)) console.log(`someone ated you!`)
		}
	};

	init(params, callbacks);
}

new Vue({
	el: '#panel',
	data() {
		return {
			userId: null,
			instance: null,
			userInput: '',
			messages: [],
		}
	},
	computed: {
		connection_status() {
			return this.instance === null ? 'loading' : "check-circle";
		}
	},
	async	mounted() {
		this.userId = `user0x` + Math.random().toString().substr(2, 4);
		const { data } = await axios.post('/api/rc/token', { id: this.userId })
		const { appKey, token } = data;
		const { getInstance, receiveNewMessage, getCurrentUser } = this;
		init({ appKey, token, imClient: null, userId: this.userId }, { getInstance, receiveNewMessage, getCurrentUser }, {});
	},
	methods: {
		getInstance(instance) {
			this.instance = instance;
			document.title = this.userId;
		},
		getCurrentUser(userId) { console.log(`userId `, userId) },
		receiveNewMessage(message) {
			console.log(message);
			this.messages.push(message)
		},
	}
})