function sendTextMessage(targetUserId, content) {
  let instance = RongIMLib.RongIMClient.getInstance();
  let pushData = "pushData" + Date.now();
  let isMentioned = false;
  let message = new RongIMLib.TextMessage({ content, extra: {} });
  instance.sendMessage(conversationType, targetUserId, message, {
    onSuccess: (message) => console.log("发送文字消息 成功"),
    onError: (errorCode, message) => console.log("发送文字消息 失败")
  }, isMentioned, pushData);
}

window.$vue = new Vue({
  el: '#app',
  data() {
    return {
      userId: 'user-3',
      instance: null,
      userInput: '',
      isTyping: false,
      targets: ['user-8', 'user-9'],
      targetUserId: 'user-8',
      connected: false,
      messages: [],
    }
  },
  computed: {
    ready() {
      return this.token !== '';
    },
    loggedIn() {
      return this.instance !== null;
    }
  },
  async mounted() {
    this.$notification.config({ placement: 'bottomLeft' });
    // await this.login();
    // await login(); // login in app.js
  },
  methods: {
    handleConnected(instance) {
      this.instance = instance;
      this.connected = true;
    },
    getCurrentUser(user) { console.log(`user `, user) },
    receiveNewMessage(message) {
      console.log(message);
      // this.$message.success(`${message.senderUserId} says ${message.content.content}`)
      this.$notification.open({ message: `${message.senderUserId} says`, description: message.content.content })
      this.messages.push(message)
    },
    async login(userId = this.userId) {
      console.log(`source, login...`)
      const { data } = await axios.post('/api/rc/token', { id: userId })
      const { appKey, token } = data;
      const { getCurrentUser, receiveNewMessage } = this;

      console.log(`call init, userId: ${userId}`)
      init({ appKey, token, imClient: null, userId }, { getInstance: this.handleConnected, receiveNewMessage, getCurrentUser }, {});

      // RongIMLib.RongIMEmoji.init();
      // RongIMLib.RongIMVoice.init();
      // console.log(`done`, this.instance)
    },
    handleUserInput() { },
    handleUserTyping() { },
    handleKeyup() {
      const vm = this;
      setTimeout(() => { vm.isTyping = false }, 5000)
    },

    disconnect() {
      this.instance.disconnect();
    },

    reconnect() {
      RongIMClient.reconnect({
        onSuccess: (userId) => console.log(`reconnect，用户id ${userId}`),
        onTokenIncorrect: () => console.log('reconnect, token无效'),
        onError: function (errorCode) {
          let info = '';
          switch (errorCode) {
            case RongIMLib.ErrorCode.TIMEOUT:
              info = '超时';
              break;
            case RongIMLib.ErrorCode.UNKNOWN_ERROR:
              info = '未知错误';
              break;
            case RongIMLib.ErrorCode.UNACCEPTABLE_PROTOCOL_VERSION:
              info = '不可接受的协议版本';
              break;
            case RongIMLib.ErrorCode.IDENTIFIER_REJECTED:
              info = 'appkey不正确';
              break;
            case RongIMLib.ErrorCode.SERVER_UNAVAILABLE:
              info = '服务器不可用';
              break;
          }
          console.log(`reconnect, error: ${info}`);
        }
      });

    },

    async logout() {
      this.instance.logout()
      this.userId = 'none'
    },

    async rename(name) {
      if (name === this.userId) {
        console.log(`same name, no updating`)
        return;
      }

      this.userId = name;
      console.log(`renamed to ${this.userId}`);

      this.instance.logout();
      await this.login();
    },

    async processUserInput() {
      let content = this.userInput.trim();
      if (content[0] === '/') {
        const command = content.substr(1, content.length - 1).split(' ');
        console.log(command)
        if (command[0] === 'rename' && command.length === 2) await this.rename(command[1])
        else if (command[0] === 'logout') await this.logout();
        else if (command[0] === 'login' && command.length === 2) {
          this.userId = command[1];
          await this.login();
          this.$message.success('logged in now ...')
        }
        else if (command[0] === 'disconnect') this.disconnect()
        else if (command[0] === 'reconnect') this.reconnect();
        else console.log(`unkown command: ${command[0]}`)
      } else {
        sendTextMessage(this.targetUserId, this.userInput);
      }

      this.userInput = ''
      this.isTyping = false;
    }
  }
})
