new Vue({
  el: '#app',

  data: function () {
    return {
      visible: false,
      hash: null,
      appKey: null,
      token: null
    }
  },

  created() {
    const el = this;
    Fingerprint2.get(function (components) {
      console.log(components)
      const values = components.map(function (component) { return component.value })
      el.hash = Fingerprint2.x64hash128(values.join(''), 31)
      console.log(`hash ${el.hash}`)
    })
  },

  methods: {
    async handleFetchRcToken() {
      this.visible = true;
      if (this.hash === null) {
        console.log(`hash is null`)
        return
      }
      const { data } = await axios.post(`/rc/token`, { id: this.hash })
      this.appKey = data.appKey;
      this.token = data.token;

      const params = {
        appKey: this.appKey,
        token: this.token,
        imClient: null,
        userId: this.hash
      };

      console.dir(params)

      const callbacks = {
        getInstance: function (_instance) {
          instance = _instance;
        },

        receiveNewMessage: function (message) {
          console.log(message);
          console.log(`新消息，类型为： $message.messageType}`);
          console.log("messageUId:" + message.messageUId + ",   messageId:" + message.messageId);
        },

        getCurrentUser: function (userInfo) {
          console.log(`链接成功`, userInfo);
          // afterConnected();
        }
      };
      const config = {}
      init(params, callbacks, config)

      RongIMLib.RongIMEmoji.init();
      RongIMLib.RongIMVoice.init();
    }
  }
})