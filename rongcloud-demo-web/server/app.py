#!/usr/bin/env python
# coding: utf8

import sys
import uuid
import asyncio
import logging
import json
from pprint import pprint as print

import sanic
from sanic import response

from avatar_generator import Avatar

# from pprint import pprint as print
import rongcloud
import jinja2

jinja2_env = jinja2.Environment(loader=jinja2.PackageLoader('app', 'templates'))

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

app = sanic.Sanic()

app.config['host'] = '106.13.74.98'
app.config['port'] = 2019
app.config['rc_app_key'] = '8luwapkv8j5xl'
app.config['rc_app_secret'] = '757xhqgEdSijt'
app.config['rc_instance'] = rongcloud.RongCloud(app.config['rc_app_key'], app.config['rc_app_secret'])

online_user_list = set()


async def rcUserToken(req):
    print(req.json)
    userId = req.json['id']
    userName = userId[:4]
    portraitUri = 'http://www.rongcloud.cn/images/logo.png'

    resp = app.config['rc_instance'].User.getToken(userId=userId, name=userName, portraitUri=portraitUri)
    return response.json({
        'token': resp.result['token'],
        'appKey': req.app.config['rc_app_key'],
        'response': resp,
    })


async def userAvatarImage(req, uid):
    avatar = Avatar.generate(128, uid)
    return response.raw(avatar, headers={'Content-Type': 'image/jpeg'})


async def index(req):
    return await response.file('./static/index.html')


async def static_files(req, file_name):
    return await response.file(f'./static/{file_name}')


async def favicon(req):
    return await response.file('./static/favicon.ico')


async def userStatusUpdate(req):
    d = {'0': 'offline', '1': 'online', '2': 'logout'}
    for user in req.json:
        # 'userid', 'status', 'os', 'time'
        logging.info('%s status: %s', req.json['userid'], d[user['status']])
        if user['status'] == '1':
            online_user_list.add(user['userid'])
        else:
            online_user_list.remove(user['userid'])

    publishSystemMessage(json.dumps(req.json))
    return response.json(req.json)


async def messageRoutesBack(req):
    print(req.raw_args)
    print(req.form)
    return response.json({})


def publishSystemMessage(content=''):
    app.config['rc_instance'].Message.PublishSystem(
        fromUserId='system',
        toUserId={"user0x0009", "user0x0000"},
        objectName='RC:TxtMsg',
        content=content,
        pushContent='thisisapush',
        pushData='{\"pushData\":\"hello\"}',
        isPersisted='0',
        isCounted='0')


def rcPublishSystemMessage(req):
    publishSystemMessage('')
    return response.json({})


def rcOnlineUserList(req):
    return response.json(online_user_list)


routes = [
    (index, '/', 'GET'),
    (favicon, '/favicon.ico', 'GET'),
    (static_files, '/static/<file_name>', 'GET'),
    (rcUserToken, '/api/rc/token', 'POST'),
    (userAvatarImage, '/api/rc/avatar/<uid>', 'GET'),
    (userStatusUpdate, '/api/rc/user-status-update', 'POST'),
    (messageRoutesBack, '/api/rc/message-routes-back', 'POST'),
    (rcPublishSystemMessage, '/api/rc/system-message', 'POST'),
    (rcOnlineUserList, '/api/rc/online-users', 'GET'),
]

for handler, uri, method in routes:
    app.add_route(handler, uri, [method])


async def broadcast(app):
    # https://www.rongcloud.cn/docs/push_service.html#push
    # 有限制
    content = {"content": "哈哈", "extra": None}
    pushContent = 'this-is-a-push'
    pushData = {'pushData': 'hello'}
    platform = 'all'
    resp = app.config['rc_instance'].Message.broadcast(
        fromUserId='userId1', objectName='RC:TxtMsg', content=json.dumps(content), pushContent=pushContent, pushData=json.dumps(pushData), os=platform)
    print(resp)


async def notify_every_five_seconds(app):
    # TODO how to stop it?
    while True:
        await asyncio.sleep(5)
        logging.info("it's been five seconds!")
        await broadcast(app)


# app.add_task(notify_every_five_seconds(app))


@app.middleware('request')
async def merge_request_data(req):
    # req['data'] = {**req.raw_args, **req.form, **req.json}
    # logging.info('data: %s', req['data'])
    pass


if __name__ == "__main__":
    print('listening on {}:{}'.format(app.config['host'], app.config['port']))
    app.run(host='localhost', port=app.config['port'], debug=True)
