import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const RongIMLib = window.RongIMLib;
RongIMLib.init('8luwapkv8j5xl', null, {});

const rc_instance = RongIMLib.RongIMClient.getInstance();

const state = {
  rc_instance,
}

const getters = {}
const mutations = {}
const actions = {}

function createDummy() {
  console.log(`create dummy plugin`)
  return store => {
    // called when the store is initialized
    store.subscribe((mutation, state) => {
      // called after every mutation.
      // The mutation comes in the format of `{ type, payload }`.
    })
  }
}

export default new Vuex.Store({ state, getters, mutations, actions, plugins: [createDummy()] })