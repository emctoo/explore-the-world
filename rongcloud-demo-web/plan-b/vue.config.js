module.exports = {
  devServer: {
    proxy: {
      '/login': {
        target: 'http://localhost:8998',
        changeOrigin: true
      },
      '/api': {
        target: 'http://localhost:8998',
        changeOrigin: true
      },
    }
  },
  configureWebpack: {
    externals: {
      'RongIMLib': 'RongIMLib'
    }
  },
}