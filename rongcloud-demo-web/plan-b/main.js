import Vue from "vue";

import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";

Vue.use(Antd);

import App from './App.vue';
import store from './store';

new Vue({ el: '#app', render: h => h(App), store })