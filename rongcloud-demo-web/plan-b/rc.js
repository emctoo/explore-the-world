const RongIMLib = window.RongIMLib;
const RongIMClient = RongIMLib.RongIMClient;
const conversationType = RongIMLib.ConversationType.PRIVATE; // 私聊

export function init($vue, { appKey, token, userId }, { getInstance, getCurrentUser, receiveNewMessage }) {
	let instance = RongIMLib.RongIMClient.getInstance();

	// 连接状态监听器
	RongIMLib.RongIMClient.setConnectionStatusListener({

		onChanged: function (status) {
			let statusMessage = '';

			switch (status) {
				case RongIMLib.ConnectionStatus["CONNECTED"]:
				case 0:
					statusMessage = 'connected'
					getInstance(instance);
					break;

				case RongIMLib.ConnectionStatus["CONNECTING"]:
				case 1:
					statusMessage = "connecting";
					break;

				case RongIMLib.ConnectionStatus["DISCONNECTED"]:
				case 2:
					statusMessage = "user disconnected"
					break;

				case RongIMLib.ConnectionStatus["NETWORK_UNAVAILABLE"]:
				case 3:
					statusMessage = "network unavailable"
					break;

				case RongIMLib.ConnectionStatus["CONNECTION_CLOSED"]:
				case 4:
					statusMessage = "unknown error, connction closed"
					break;

				case RongIMLib.ConnectionStatus["KICKED_OFFLINE_BY_OTHER_CLIENT"]:
				case 6:
					statusMessage = "kicked off by other client"
					break;

				case RongIMLib.ConnectionStatus["DOMAIN_INCORRECT"]:
				case 12:
					statusMessage = "当前运行域名错误，请检查安全域名配置"
					break;
			}

			$vue.connectionStatus = statusMessage;
		}
	});

	/*
	文档：http://www.rongcloud.cn/docs/web.html#3、设置消息监听器

	注意事项：
		1：为了看到接收效果，需要另外一个用户向本用户发消息
		2：判断会话唯一性 ：conversationType + targetId
		3：显示消息在页面前，需要判断是否属于当前会话，避免消息错乱。
		4：消息体属性说明可参考：http://rongcloud.cn/docs/api/js/index.html
	*/
	RongIMClient.setOnReceiveMessageListener({ onReceived: receiveNewMessage });

	// 开始链接
	function onSuccess(userId) {
		getCurrentUser({ userId });
		console.log(`connecting success, userId: ${userId}`);
	}

	function onTokenIncorrect() {
		console.log('connecting error, token invalid');
	}

	function onError(errorCode) {
		console.log(`connecting error, error code`, errorCode);
	}

	console.log(`rc init now, userId: ${userId}`)
	RongIMClient.connect(token, { onSuccess, onTokenIncorrect, onError }, userId);
}

export function sendTextMessage(targetUserId, content) {
	let instance = RongIMLib.RongIMClient.getInstance();
	let pushData = "pushData" + Date.now();
	let isMentioned = false;
	let message = new RongIMLib.TextMessage({ content, extra: {} });
	instance.sendMessage(conversationType, targetUserId, message, {
		onSuccess: (message) => console.log("发送文字消息 成功"),
		onError: (errorCode, message) => console.log("发送文字消息 失败")
	}, isMentioned, pushData);
}