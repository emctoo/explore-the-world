const RongIMLib = window.RongIMLib;

export function sendMessage(targetUserId, content) {
  let instance = RongIMLib.RongIMClient.getInstance();
  let pushData = "pushData" + Date.now();
  let isMentioned = false;
  let message = new RongIMLib.TextMessage({ content, extra: {} });
  let start = new Date().getTime();
  instance.sendMessage(conversationType, targetUserId, message, {
    onSuccess: function (message) {
      console.log("发送文字消息 成功", message, start);
    },
    onError: function (errorCode, message) {
      console.log("发送文字消息 失败", message, start);
    }
  }, isMentioned, pushData);
}

const funcs = {
  'help': ($vue, argv) => {
    console.log('help', argv)
  },

  'switch-user': ($vue, argv) => { },

  'rename': async ($vue, argv) => {
    if ($vue.userId === argv[0]) return;
    $vue.userId = argv[0];
    await $vue.login()
  }
}

export function commands($vue, content) {
  if (content[0] === '/') {
    const argv = content.substr(1, content.length - 1).split(' ');
    funcs[argv[0]]($vue, argv.slice(1, argv.length))
  }

  return false;
}

export async function fingerprint() {
  return new Promise((resolve) => {
    Fingerprint2.get((components) => {
      const values = components.map(function (component) { return component.value })
      resolve(Fingerprint2.x64hash128(values.join(''), 31))
    })
  })
}

function disconnect() {
  // 文档：http://www.rongcloud.cn/docs/api/js/RongIMClient.html
  RongIMLib.RongIMClient.getInstance().disconnect();
}

function reconnect(userId) {
  // http://www.rongcloud.cn/docs/api/js/RongIMClient.html
  // 1: reconnect 是重新连接，并没有重连机制，调用此方法前应该进行网络嗅探，网络正常再调用 reconnect。
  // 2: 提示其他设备登录请勿调用重连方法。

  RongIMClient.reconnect({
    onSuccess: function (userId) { },
    onTokenIncorrect: function () { },
    onError: function (errorCode) {
      let info = '';
      switch (errorCode) {
        case RongIMLib.ErrorCode.TIMEOUT:
          info = '超时';
          break;
        case RongIMLib.ErrorCode.UNKNOWN_ERROR:
          info = '未知错误';
          break;
        case RongIMLib.ErrorCode.UNACCEPTABLE_PROTOCOL_VERSION:
          info = '不可接受的协议版本';
          break;
        case RongIMLib.ErrorCode.IDENTIFIER_REJECTED:
          info = 'appkey不正确';
          break;
        case RongIMLib.ErrorCode.SERVER_UNAVAILABLE:
          info = '服务器不可用';
          break;
      }
    }
  });
}