#!/usr/bin/env python
import pika
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)

params = {
    'host': '127.0.0.1',
    'port': 5672,
    'virtual_host': 'admin',
    'credentials': pika.credentials.PlainCredentials('admind', '8053+closer-dev'),
    'client_properties': {
        'connection_name': 'hello_world',
    },
}

# url: pika.URLParameters = pika.URLParameters('amqp://{username}:{password}@{host}/{vhost}'.format(**params))
conn_params: pika.ConnectionParameters = pika.ConnectionParameters(**params)
connection: pika.BlockingConnection = pika.BlockingConnection(conn_params)  # (url)

channel: pika.channel.Channel = connection.channel()  # 连接复用
channel.queue_declare(queue='hello')


def callback(ch, method, properties, body):
    logger.info("Received [%r]" % body)


# queue='hello' 必须是存在的, 所以上面 queue_declare
# auto_ack
# basic_consume
channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

try:
    logger.info('Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()  # 阻塞

except KeyboardInterrupt:
    print('\rbye.')
