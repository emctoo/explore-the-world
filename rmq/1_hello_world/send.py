#!/usr/bin/env python
import pika

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)

params = {
    'host': '127.0.0.1',
    'port': 5672,
    'virtual_host': 'admin',
    'vhost': 'admin',
    'credentials': pika.credentials.PlainCredentials('admind', '8053+closer-dev'),
    'client_properties': {
        'connection_name': 'hello_world',
    },
}

# url: pika.URLParameters = pika.URLParameters('amqp://{username}:{password}@{host}/{vhost}'.format(**params))
conn_params: pika.ConnectionParameters = pika.ConnectionParameters(**params)
connection: pika.BlockingConnection = pika.BlockingConnection(conn_params)  # (url)

channel: pika.channel.Channel = connection.channel()
channel.queue_declare(queue='hello')

# 默认 exhange=''
channel.basic_publish(exchange='', routing_key='hello', body='Hello World!', properties=None, mandatory=False)
logger.info("Sent 'Hello World!'")
connection.close()
