#!/usr/bin/env python
# coding; utf8

import os
os.path.append(os.path.abspath('../utils'))

import pika
import rabbit

from rabbit import logger

"""接收广播消息"""

connection = rabbit.create_connection(connection_name='3-ps:receive-logs')
channel = connection.channel()

channel.exchange_declare(exchange='logs', exchange_type='fanout')

result = channel.queue_declare(queue='', exclusive=True) # 自动生成的名字
queue_name = result.method.queue

channel.queue_bind(exchange='logs', queue=queue_name)


def callback(ch, method, properties, body):
    logger.info("body: %r", body)

channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

logger.info('waiting for logs ...')
channel.start_consuming()

