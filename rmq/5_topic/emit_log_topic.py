#!/usr/bin/env python
# coding: utf8

import sanic
from sanic import response

import time, uuid, random, json, sys, logging
import pika

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger = logging.getLogger('emit')

import rabbit

app = sanic.Sanic()


@app.listener('before_server_start')
async def before_start(app, loop):
    app.conn: pika.channel.Channel = rabbit.create_connection('emit_log_topic')
    app.channel: pika.channel.Channel = app.conn.channel()
    app.channel.exchange_declare(exchange='topic_logs', exchange_type='topic')


@app.listener('before_server_stop')
async def before_stop(app, loop):
    app.conn.close()


@app.middleware('request')
async def request_hook(req):
    req['request_id'] = str(uuid.uuid4())
    req['query'] = req.query_args or req.json or req.form


@app.middleware('response')
async def response_hook(req, resp):
    payload = {
        'request_id': req['request_id'],
        'query': req['query'],
        'headers': dict(req.headers),
    }
    body = json.dumps(payload).encode()
    req.app.channel.basic_publish(exchange='topic_logs', routing_key='requests', body=body)


@app.route('/', methods=['POST', 'GET'])
async def index(req):
    logger.info('query: %s', req['query'])
    return response.text('hello')


def work(routing_key):

    with rabbit.create_channel(connection_name='emit_log_topic') as channel:
        channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

        message = ' '.join(sys.argv[2:]) or 'Hello World!'

        message = "{} {} hello".format(time.time(), str(uuid.uuid4()))
        channel.basic_publish(exchange='topic_logs', routing_key=routing_key, body=message)
        logger.info("sent %s, %s", routing_key, message)


if __name__ == "__main__":
    # routing_key = sys.argv[1] if len(sys.argv) > 2 else 'anonymous.info'
    try:
        # work('anonymous.info')
        app.run(auto_reload=True)
    except KeyboardInterrupt:
        print('\rbye')