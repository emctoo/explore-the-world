#!/usr/bin/env python
# coding: utf8

import contextlib
import pika

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger = logging.getLogger(__name__)


def create_connection(connection_name):
    params = {
        'host': '127.0.0.1',
        'port': 5672,
        'virtual_host': 'admin',
        'credentials': pika.credentials.PlainCredentials('admind', '8053+closer-dev'),
        'client_properties': {
            'connection_name': connection_name,
        },
    }

    return pika.BlockingConnection(pika.ConnectionParameters(**params))


class create_channel(contextlib.ContextDecorator):

    def __init__(self, *, connection_name):
        self.params = {
            'host': '127.0.0.1',
            'port': 5672,
            'virtual_host': 'admin',
            'credentials': pika.credentials.PlainCredentials('admind', '8053+closer-dev'),
            'client_properties': {
                'connection_name': connection_name,
            },
        }

    def __enter__(self):
        self.connection: pika.BlockingConnection = pika.BlockingConnection(pika.ConnectionParameters(**self.params))
        self.channel: pika.channel.Channel = self.connection.channel()
        logger.info('channel_number: %s', self.channel.channel_number)
        return self.channel

    def __exit__(self, *exc):
        self.connection.close()
        return False
