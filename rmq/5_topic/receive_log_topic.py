#!/usr/bin/env python
# coding: utf8

import pika
import sys

import rabbit

import argparse

from systemd import journal

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger = logging.getLogger(__name__)


def callback(ch, method, properties, body):
    logger.info("routing_key: %r, body: %r", method.routing_key, body)


def main(binding_keys):
    with rabbit.create_channel(connection_name='receive_log_topic') as channel:  # type: pika.channel.Channel

        channel.exchange_declare(exchange='topic_logs', exchange_type='topic')  # 注意名字和类型

        result = channel.queue_declare('', exclusive=True)  # 自动生成queue name, connection exclusive
        logger.info('queue declare result: %s', result)

        queue_name = result.method.queue

        for binding_key in binding_keys:
            channel.queue_bind(exchange='topic_logs', queue=queue_name, routing_key=binding_key)  # routing_key 可以有 # 和 * 通配符

        logger.info('waiting for [%s] logs ...', ', '.join(binding_keys))

        channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)

        channel.start_consuming()


if __name__ == "__main__":
    # TODO argument parser
    parser = argparse.ArgumentParser('log receiver')
    parser.add_argument('keys')

    binding_keys = sys.argv[1:] or ['*.info']

    try:
        main(binding_keys)
    except KeyboardInterrupt:
        print('\rbye.')