#!/usr/bin/env python
# coding: utf8

# from cysystemd import journal

import logging
import subprocess
import json
import asyncio

import aio_pika

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

URL = 'amqp://admin:17380538161@127.0.0.1'

async def publish_message(content, url=URL, routing_key='journalctl', loop=None):
    if loop is None:
        loop = asyncio.get_event_loop()

    message = aio_pika.Message(body='hello, world!'.encode())

    conn = await aio_pika.connect_robust(url, loop=loop)
    async with conn:
        channel = await conn.channel()
        await channel.default_exchange.publish(message, routing_key=routing_key)


async def main():
    command = ['journalctl', '-f']
    journalctl_process = subprocess.Popen(command, bufsize=0, stdout=subprocess.PIPE)
    while journalctl_process.poll() is None:
        content = journalctl_process.stdout.readline().decode().strip()
        # print(json.loads(content))
        if content.startswith('--') and content.endswith('--'):
            continue
        await publish_message(content)

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())

