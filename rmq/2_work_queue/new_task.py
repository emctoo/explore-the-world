#!/usr/bin/env python
# coding: utf8

import pika
import sys

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger = logging.getLogger('emit')

import rabbit
import server


async def response_hook(req, resp):
    message = 'hello, world!'

    properties = pika.BasicProperties(delivery_mode=2)  # delivery_mode=2 : message persistent
    req.app.channel.basic_publish(exchange='', routing_key='task_queue', body=message, properties=properties)


server.app.register_middleware(response_hook, attach_to='response')

if __name__ == '__main__':
    server.app.run(auto_reload=True)