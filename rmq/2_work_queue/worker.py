#!/usr/bin/env python
# coding: utf8

import pika
import time

import rabbit

import logging
logger = logging.getLogger(__name__)


class Scheduler:
    QUEUE_NAME = 'task_queue'

    def __init__(self):
        self.connection: pika.BlockingConnection = rabbit.create_connection('worker')
        self.channel: pika.channel.Channel = self.connection.channel()

        self.channel.queue_declare(queue=Scheduler.QUEUE_NAME, durable=True)

        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(queue=Scheduler.QUEUE_NAME, on_message_callback=self.callback)

    def callback(self, ch, method, properties, body):
        logger.info("received %r" % body)
        time.sleep(body.count(b'.'))
        logger.info("done")

        ch.basic_ack(delivery_tag=method.delivery_tag)

    def run(self):
        logger.info('waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()


if __name__ == "__main__":
    scheduler = Scheduler()
    try:
        scheduler.run()
    except KeyboardInterrupt:
        print('\rbye.')
