#!/usr/bin/env python
# coding: utf8

import os, sys
sys.path.append(os.path.abspath('../utils'))

import pika
import functools
import time
import json

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)

import rabbit

@functools.lru_cache(maxsize=32)
def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


def on_request(ch, method, props, buf):
    body = json.loads(buf)
    logger.info('request: %s', body)
    n = body['params']['n']

    logger.info("compute fib(%s)", n)
    start_timestamp = time.time_ns()
    response = fib(n)
    logger.info('result with timeit: %s, %sns', response, time.time_ns() - start_timestamp)

    logger.info('reply_to: %s, correlation_id: %s', props.reply_to, props.correlation_id)
    properties = pika.BasicProperties(correlation_id=props.correlation_id)
    ch.basic_publish(exchange='rpc', routing_key=props.reply_to, properties=properties, body=str(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)


def main():
    with rabbit.create_channel(connection_name='rpc_server') as channel:
        channel.queue_declare(queue='rpc_queue')

        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue='rpc_queue', on_message_callback=on_request)

        logger.info("awaiting RPC requests ...")
        channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\rbye.')
