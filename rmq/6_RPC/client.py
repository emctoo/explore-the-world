#!/usr/bin/env python
# coding: utf8

import os, sys
sys.path.append(os.path.abspath('../utils/'))

import rabbit

import contextvars
import pika
import uuid
import random
import time
import json

import sanic
from sanic import response

import logging

handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('rpc_client - %(asctime)s - %(message)s'))

logger = logging.getLogger('rpc')
logger.addHandler(handler)
logger.propagate = False

class RmqClient(object):

    def __init__(self):
        self._setup_rpc_client()

    def _setup_rpc_client(self):
        self.connection: pika.BlockingConnection = rabbit.create_connection('rpc_client')
        self.channel: pika.channel.Channel = self.connection.channel()

        self.exchange = 'rpc'
        self.channel.exchange_declare(self.exchange, exchange_type='direct')

        # call
        self.call_queue_name = 'rpc_queue'
        self.channel.queue_bind(exchange=self.exchange, queue='rpc_queue', routing_key='rpc_queue')

        # result
        self.result_queue_name = 'rpc_result_queue'
        result = self.channel.queue_declare(queue=self.result_queue_name)  # TODO 返回类型
        self.callback_queue = result.method.queue
        logger.info('callback queue: %s', self.callback_queue)

        self.channel.queue_bind(queue=self.callback_queue, exchange=self.exchange, routing_key=self.callback_queue)

        # consume result message, auto_ack
        self.channel.basic_consume(queue=self.callback_queue, on_message_callback=self.on_response, auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.correlation_id == props.correlation_id:
            self.response = body

    def call(self, n): # TODO func, params
        self.response = None
        self.correlation_id = str(uuid.uuid4()) # 每次都用不同的 request_id
        routing_key = self.call_queue_name
        try:
            result = self._publish(routing_key=self.call_queue_name, func='fib', params={'n': n})
        except pika.exceptions.AMQPError as e:
            logger.info('error happens, %s', e)

    def _publish(self, *, routing_key, func, params):
        properties_dict = {
            'reply_to': self.callback_queue,
            'correlation_id': self.correlation_id,
        }
        properties = pika.BasicProperties(**properties_dict)

        body = json.dumps({'func': func, 'params': params})
        self.channel.basic_publish(exchange=self.exchange, routing_key=routing_key, properties=properties, body=body)

        while self.response is None:
            self.connection.process_data_events() # TODO ?
        return self.response


def _main():

    rpc_client = RmqClient()

    for _ in range(10):
        number = random.randint(0, 100)
        logger.info("Requesting fib(%d) ...", number)
        response = fibonacci_rpc.call(number)
        logger.info("fib(%d) => %r", number, response)
        time.sleep(10)

app = sanic.Sanic()

_rmq_client = None
rmq_client = contextvars.ContextVar('rmq_client')

@app.listener('after_server_start')
async def before_start(app, loop):
    global _rmq_client, rmq_client
    _rmq_client = RmqClient()
    # rmq_client.set(_rmq_client)

@app.middleware('request')
async def before_request(req):
    rmq_client.set(_rmq_client)


@app.route('/fib/<n:int>')
async def index(req, n):
    logger.info('visit fib(%d).', n)
    result = rmq_client.get().call(n)
    return response.json({'func': 'fib', 'params': {'n': n}, 'result': result})


if __name__ == "__main__":
    # main()
    logger.info('start serving ...')
    app.run(auto_reload=True)
