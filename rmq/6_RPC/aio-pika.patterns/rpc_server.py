import asyncio
from aio_pika import connect_robust
from aio_pika.patterns import RPC

import functools

import conf

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)


async def multiply(*, x, y):
    logger.info('multiply(%d, %d)', x, y)
    return x * y


async def fib(*, x=3):
    logger.info('fib(%d)', x)
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)


async def main():
    connection = await connect_robust(conf.URL)
    channel = await connection.channel()

    rpc: RPC = await RPC.create(channel)

    logger.info('register multiply call ...')
    await rpc.register('multiply', multiply, auto_delete=True)

    logger.info('register fib call ...')
    await rpc.register('fib', fib, auto_delete=True)

    return connection


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    connection = loop.run_until_complete(main())

    try:
        logger.info('serving ...')
        loop.run_forever()
    except KeyboardInterrupt:
        print('\rbye.')
    finally:
        loop.run_until_complete(connection.close())
        # loop.shutdown_asyncgens()
