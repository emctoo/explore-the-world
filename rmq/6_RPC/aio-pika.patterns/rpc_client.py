import asyncio
from aio_pika import connect_robust
from aio_pika.patterns import RPC

import conf
import random
import time

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger: logging.Logger = logging.getLogger(__name__)

logging.getLogger('aio_pika.patterns.rpc').setLevel(logging.DEBUG)

async def main():

    logger.info('connecting ...')
    connection = await connect_robust(conf.URL)

    async with connection:
        channel = await connection.channel()

        rpc: RPC = await RPC.create(channel)

        x = random.randint(0, 100)
        iters = 3

        for i in range(iters):
            logger.info('call multiply(%d, %d)', x, i)

            start = time.time_ns()
            result = await rpc.proxy.multiply(x=x, y=i)
            logger.info('multiply(%d, %d) => %d, %dns', x, i, result, time.time_ns() - start)

        # for i in range(iters):
        #     logger.info('* call multiply(%d, %d)', x, i)

        #     start = time.time_ns()
        #     result = await rpc.call('multiply', kwargs=dict(x=x, y=i))
        #     logger.info('* multiply(%d, %d) => %d, %dns', x, i, result, time.time_ns() - start)

        logger.info('x: %d', x)
        await rpc.call('fib', kwargs=dict(x=x))
        # logger.info('fib(%d) => %d, %dns', x, result, time.time_ns() - start)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        print('\rbye.')
