#!/usr/bin/env python
# coding: utf8

import uuid
import rabbit

import sanic
from sanic import response

import logging
logger = logging.getLogger(__name__)

app = sanic.Sanic()


async def setup_rmq(app, loop):
    app.connection: pika.BlockingConnection = rabbit.create_connection(connection_name='new_task')
    app.channel: pika.channel.Channel = app.connection.channel()

    app.channel.queue_declare(queue='task_queue', durable=True)


async def cleanup_rmq(app, loop):
    app.connection.close()


async def request_hook(req):
    req['request_id'] = str(uuid.uuid4())


app.register_listener(setup_rmq, 'before_server_start')
app.register_listener(cleanup_rmq, 'after_server_stop')
app.register_middleware(request_hook, 'request')

