<template>
  <div
    v-if="showing"
    class="card"
    v-bind:class="{ animated: animating, current }"
    v-bind:style="{ transform: returnTransformString }"
  >
    <div class="image" v-bind:style="{ backgroundImage: returnImageString }">
      <div class="image-icon" v-bind:class="icon.type" v-bind:style="{ opacity: icon.opacity }"></div>
    </div>

    <h1 class="name">{{ fullName }}</h1>

    <div class="stars">
      <div
        v-for="(star, index) in maxStars"
        :key="index"
        v-bind:class="[(rating > index) ? 'star-active' : 'star-inactive']"
      ></div>
    </div>
  </div>
</template>

<script>
import interact from "interactjs";

export default {
  props: {
    current: {
      type: Boolean,
      required: true
    },
    fullName: {
      type: String,
      required: true
    },
    picture: {
      type: String,
      required: false
    },
    rating: {
      type: Number,
      required: true
    },
    approved: {
      type: Boolean
    }
  },
  data: function() {
    return {
      showing: true,
      maxStars: 5,
      animating: true, // Controls CSS class with transition declaration
      threshold: window.innerWidth / 3, // Breakpoint distance to approve/reject a card
      maxRotation: 10, // Max rotation value in degrees
      position: { x: 0, y: 0, rotation: 0 },
      icon: { opacity: 0, type: null }
    };
  },
  computed: {
    returnImageString: function() {
      return `url(${this.picture})`;
    },

    returnTransformString: function() {
      if (this.animating === false || this.approved !== null) {
        const x = this.position.x;
        const y = this.position.y;
        const rotate = this.position.rotation;
        return `translate3D(${x}px, ${y}px, 0) rotate(${rotate}deg)`;
      }

      return null;
    }
  },

  mounted: function() {
    const element = this.$el;
    const self = this;

    interact(element).draggable({
      inertia: false,

      onstart: function() {
        self.animating = false; // Disable CSS transitions during dragging.
      },

      onmove: function(event) {
        /*
          Calculate new x and y coordinate values from the local value and the event object value.
          Also adjust element rotation transformation based on proximity to approve/reject threshold.
				*/

        const x = (self.position.x || 0) + event.dx;
        const y = (self.position.y || 0) + event.dy;

        let rotate = self.maxRotation * (x / self.threshold);

        if (rotate > self.maxRotation) {
          rotate = self.maxRotation;
        } else if (rotate < -self.maxRotation) {
          rotate = -self.maxRotation;
        }

        self.position.x = x;
        self.position.y = y;
        self.position.rotation = rotate;

        /*
          Change icon image type based on drag direction and adjust opacity from 0-1 based on current rotation amount.
          Also emit an event to show/hide respective button below cards during dragging.
				*/

        if (rotate > 0) {
          self.icon.type = "approve";
        } else if (rotate < 0) {
          self.icon.type = "reject";
        }

        const opacityAmount = Math.abs(rotate) / self.maxRotation;

        self.icon.opacity = opacityAmount;
        self.$emit("draggedActive", self.icon.type, opacityAmount);
      },
      onend: function(event) {
        /*
          Check if card has passed the approve/reject threshold and emit approval value change event,
          otherwise reset card and icon to default values.
				*/

        self.animating = true;

        if (self.position.x > self.threshold) {
          self.$emit("draggedThreshold", true);
          self.icon.opacity = 1;
        } else if (self.position.x < -self.threshold) {
          self.$emit("draggedThreshold", false);
          self.icon.opacity = 1;
        } else {
          self.position.x = 0;
          self.position.y = 0;
          self.position.rotation = 0;
          self.icon.opacity = 0;
        }

        self.$emit("draggedEnded");
      }
    });
  },

  watch: {
    approved: function() {
      if (this.approved !== null) {
        const self = this;

        // Remove interact listener to prevent further dragging
        interact(this.$el).unset();
        this.animating = true;

        /*
					Move card off-screen in direction of approve/reject status,
					then remove it from the DOM, thereby adjusting the CSS nth-child selectors.
				*/

        const x =
          window.innerWidth + window.innerWidth / 2 + this.$el.offsetWidth;

        if (this.approved === true) {
          this.position.x = x;
          this.position.rotation = this.maxRotation;
          this.icon.type = "approve";
        } else if (this.approved === false) {
          this.position.x = -x;
          this.position.rotation = -this.maxRotation;
          this.icon.type = "reject";
        }

        this.icon.opacity = 1;

        setTimeout(function() {
          self.showing = false;
        }, 200);
      }
    }
  }
};
</script>

<style lang="scss" scoped>
/* CONSTANTS */

$cardsTotal: 3;
$cardsWidth: 420px;
$cardsHeight: 550px;
$cardsPositionOffset: 10px;
$cardsScaleOffset: 0.02;

/* COLOURS */

$colour-white: #ffffff;
$colour-orange: #f0a435;
$colour-grey: #6e6e6e;
$colour-text: #444444;
$colour-background: #f3f3f3;

/* EXTENDS */

%backgroundContain {
  background: center center no-repeat transparent;
  background-size: contain;
}

/* CARD */

$defaultTranslation: $cardsPositionOffset * $cardsTotal;
$defaultScale: 1 - ($cardsScaleOffset * $cardsTotal);

.card {
  pointer-events: none;
  z-index: 0;
  opacity: 0;
  left: 0;
  top: 0;
  position: absolute;
  padding: 15px 15px 30px 15px;
  width: 420px;
  height: $cardsHeight;
  border-radius: 8px;
  background: $colour-white;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
  transform: translateY($defaultTranslation) scale($defaultScale);
  transform-origin: 50%, 100%;
  will-change: transform, opacity;
}

/*
	Cascade the cards by translation and scale based on their nth-child index
*/

@for $i from 1 through $cardsTotal {
  $index: $i - 1;
  $translation: $cardsPositionOffset * $index;
  $scale: 1 - ($cardsScaleOffset * $index);

  .card:nth-child(#{$i}) {
    opacity: 1;
    z-index: $cardsTotal - $index;
    transform: translateY($translation) scale($scale);
  }
}

.card.current {
  pointer-events: auto;
}

.card.animated {
  transition: all 0.5s cubic-bezier(0.175, 0.885, 0.32, 1.275);
}

.card > .image {
  margin: 0 auto 30px 0;
  width: 420px - (15px * 2);
  height: 420px - (15px * 2);
  @extend %backgroundContain;
}

$imageIconSize: 200px;

.card > .image > .image-icon {
  position: relative;
  left: 50%;
  top: 50%;
  width: $imageIconSize;
  height: $imageIconSize;
  transform: translateX(-50%) translateY(-50%);
  background: center center no-repeat transparent;
  background-size: contain;
}

.card > .image > .image-icon.approve {
  background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/617753/icon-approve.svg");
}

.card > .image > .image-icon.reject {
  background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/617753/icon-reject.svg");
}

.card > .name {
  margin: 0 auto 30px 0;
  display: block;
  font-size: 24px;
  line-height: 24px;
  text-align: center;
  text-transform: capitalize;
}

$starSize: 24px;
$starSpacing: $starSize;
$starTotal: 5;

.card > .stars {
  margin: 0 auto 0 auto;
  width: ($starSize * $starTotal) + ($starSpacing * ($starTotal - 1));
}

.card > .stars:after {
  content: "";
  display: table;
  clear: both;
}

.card > .stars > .star-active,
.card > .stars > .star-inactive {
  float: left;
  margin-right: $starSpacing;
  width: $starSize;
  height: $starSize;
  @extend %backgroundContain;
}

.card > .stars > .star-active:last-child,
.card > .stars > .star-inactive:last-child {
  margin-right: 0;
}

.card > .stars > .star-active {
  background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/617753/star-active.svg");
}

.card > .stars > .star-inactive {
  background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/617753/star-inactive.svg");
}
</style>
