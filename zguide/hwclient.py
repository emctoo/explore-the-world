#
#   Hello World client in Python
#   Connects REQ socket to tcp://localhost:5555
#   Sends "Hello" to server, expects "World" back
#

import time
import zmq
import uuid
import logging


NAME = str(uuid.uuid4())
logging.basicConfig(level=logging.DEBUG, format=f'%(asctime)s - [{NAME}] - %(message)s')


class HelloWorldClient:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.context = zmq.Context()

        #  Socket to talk to server
        self.socket: zmq.Socket = self.context.socket(zmq.REQ) # pylint: disable=E1101

        logging.info("Connecting to hello world server...")
        self.socket.connect("tcp://localhost:5555")

        self.bus_socket: zmq.Socket = self.context.socket(zmq.SUB) # pylint: disable=E1101
        self.bus_socket.connect('tcp://localhost:1921')
        self.bus_socket.setsockopt(zmq.SUBSCRIBE, b'') # pylint: disable=E1101

        self.poller: zmq.Poller = zmq.Poller()
        self.poller.register(self.bus_socket, zmq.POLLIN)



    def run(self):
        while True:
            try:
                events = dict(self.poller.poll(10))
            except KeyboardInterrupt:
                print('\rbye.')
                break

            request = time.time()
            logging.info("Sending request %s ..." % request)
            self.socket.send(b"Hello")

            #  Get the reply.
            message = self.socket.recv()
            logging.info("Received reply %s [ %s ]" % (request, message))

if __name__ == '__main__':
    client = HelloWorldClient()
    client.run()