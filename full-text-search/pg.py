#!/usr/bin/env python
# coding=utf-8

import sys
import argparse
import logging
import json

import psycopg2
import conf

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
parser = argparse.ArgumentParser('search', description='full text search driver')


def main():

    with open('/tmp/xy-data/jryw.json', 'r') as handle:
        values = json.load(handle)

    logging.info('values size: %s, keys: %s', len(values), values[0].keys())
    logging.info('value[0]: %s', values[0])

    query = """insert into posts(category, url, title, content) values('jrym', %(url)s, %(title)s, %(content)s);"""

    with psycopg2.connect(conf.PG_DSN) as connect, connect.cursor() as cursor:
        cursor.executemany(query, values)


def full_text_search(text):
    query = """select id, title, url, tsv_column from posts where tsv_column @@ to_tsquery('chinese_parser', %(target_text)s);"""
    values = {'target_text': text}

    with psycopg2.connect(conf.PG_DSN) as connect, connect.cursor() as cursor:
        cursor.execute(query, values)
        rows = cursor.fetchall()

    rows = [dict(zip(['id', 'title', 'url', 'tsv_column'], row)) for row in rows]
    print(rows[0])


if __name__ == "__main__":
    # main()
    full_text_search(sys.argv[1])