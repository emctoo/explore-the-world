#!/usr/bin/env python
# coding=utf-8

import sys
import json
import logging
import hashlib
import elasticsearch
import prompt_toolkit

logger = logging.getLogger(__name__)

es = elasticsearch.Elasticsearch(hosts=[{'host': '192.168.1.43', 'port': 9200}])

index_name = 'posts'


def setup(index_name='posts'):
    is_index_exists = es.indices.exists('posts')
    logger.info('`%s` exists: %s', index_name, is_index_exists)
    if not is_index_exists:
        result = es.indices.create(index=index_name, ignore=400)  # create `posts` index
        print(result)

    # index = es.indices.get(index='posts')
    # es.indices.delete(index='posts', ignore=[400, 404])  # delete

    with open('/tmp/xy-data/jryw.json', 'r') as handle:
        values = json.load(handle)

    value = values[0]
    print(value)

    suffix = '''.ExternalClass#contentTextP{margin-bottom:20px;font-size:16pt;font-family:宋体;line-height:30px;text-indent:2em;}.ExternalClass#contentTextIMG{height:auto;margin:0pxauto20px;display:block;width:800px;}.ExternalClassTD.content{padding-top:9px;}.ExternalClass#ctl00_PlaceHolderMain_Title__ControlWrapper_RichHtmlField{font-size:18px;font-weight:normal;}'''

    # md5防止重复插入
    for value in values:
        if value['content'].endswith(suffix):
            value['content'] = value['content'].rstrip(suffix)

        try:
            result = es.create(index=index_name, doc_type='jryw', id=hashlib.md5(value['content'].encode()).hexdigest(), body=value)
            print(result)
        except elasticsearch.exceptions.ConflictError as ex:
            print(ex)


def put_mapping(index='posts'):
    mapping = {'properties': {'title': {'type': 'text', 'analyzer': 'ik_max_word', 'search_analyzer': 'ik_max_word'}}}
    result = es.indices.put_mapping(index=index, doc_type='politics', body=mapping)


def main():
    while True:
        search_content = prompt_toolkit.prompt('> ')
        print(search_content)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('\rbye.')
