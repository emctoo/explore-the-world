package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	zmq "github.com/pebbe/zmq4"
)

var (
	upgrader = websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}
)
var subscribeURL string
var filter string

func xpubServer() {
	frontend, _ := zmq.NewSocket(zmq.XPUB)
	frontend.Bind("tcp://*:5559")

	backend, _ := zmq.NewSocket(zmq.XSUB)
	backend.Bind("tcp://*:5560")

	log.Println("serving xsub/xpub service now ...")
	zmq.Proxy(frontend, backend, nil)

	frontend.Close()
	backend.Close()
}

func ws(ctx echo.Context) error {
	ws, err := upgrader.Upgrade(ctx.Response(), ctx.Request(), nil)
	if err != nil {
		log.Printf("upgrade websocket error.\n")
		return err
	}
	defer ws.Close()

	subscriber, _ := zmq.NewSocket(zmq.SUB)
	defer subscriber.Close()

	subscriber.Connect(subscribeURL)
	subscriber.SetSubscribe(filter)

	for {
		payload, _ := subscriber.Recv(0)
		err := ws.WriteMessage(websocket.TextMessage, []byte(payload))
		if err != nil {
			ctx.Logger().Error(err)
			break
		}
	}

	log.Println("client exit.")
	return nil
}

func main() {
	host := flag.String("host", "127.0.0.1", "ws host")
	port := flag.Int("port", 9998, "w port")

	flag.StringVar(&subscribeURL, "sub", "ipc:///tmp/control-pub.ipc", "zmq subscribe url")
	flag.StringVar(&filter, "filter", "", "zmq subscribe filter")

	flag.Parse()

	go xpubServer()

	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/signaling", ws)
	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%d", *host, *port)))
}
