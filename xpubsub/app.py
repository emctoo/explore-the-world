#!/usr/bin/env python
# coding=utf-8

import sys
import zmq
import time
import threading
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(threadName)s - %(message)s')

class Poller(threading.Thread):

    def __init__(self, name, id, topic):
        super().__init__()
        self.name = name
        self.id = id
        self.topic = topic

    def run(self):
        logging.info('start poller {}'.format(self.id))
        subscriber = zmq.Context.instance().socket(zmq.SUB)
        subscriber.connect("tcp://127.0.0.1:5559")
        subscriber.setsockopt_string(zmq.SUBSCRIBE, self.topic)
        self.loop = True
        while self.loop:
            message = subscriber.recv_string()
            logging.info('poller {}: {}'.format(self.id, message))

    def stop(self):
        self.loop = False


def main():
    context = zmq.Context.instance()

    logging.info('create NASDA thread ...')
    poller1 = Poller('NASDA', 1, 'NASDA')
    poller1.start()

    logging.info('create NASDAQ thread ...')
    poller2 = Poller('NASDAQ', 2, 'NASDAQ')
    poller2.start()

    logging.info('create new zmq.PUB socket ...')
    socket = context.socket(zmq.PUB)
    socket.connect("tcp://127.0.0.1:5560") # as consumer, connect to backend

    for index in range(3):
        time.sleep(2)
        socket.send_string('NASDA:' + time.strftime('%H:%M:%S'))

        time.sleep(2)
        socket.send_string('NASDAQ:' + time.strftime('%H:%M:%S'))

    poller1.stop()
    poller2.stop()

    socket.send_string('NASDAQ:STOP')
    sys.exit(0)


if __name__ == "__main__":
    main()