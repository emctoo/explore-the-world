#!/usr/bin/env python
# coding=utf-8

import sanic
from sanic import response

app = sanic.Sanic()


async def index(req):
    return response.json({'success': True, 'payload': 'ok'})


app.add_route(index, '/')
if __name__ == '__main__':
    app.run(host='localhost', port=2000, debug=True)