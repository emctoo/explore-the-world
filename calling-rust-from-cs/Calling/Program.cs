﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace rust_dll_poc {
    [StructLayout(LayoutKind.Sequential)]
    public struct SampleStruct {
        public Int16 field_one;
        public Int32 field_two;
        public IntPtr string_field;
    }

    class Program {
        [DllImport("../source/target/release/libour_rust.so")]
        private static extern SampleStruct get_simple_struct();

        [DllImport("../source/target/release/libour_rust.so")]
        private static extern SampleStruct free_string();

        [DllImport("../source/target/release/libour_rust.so")]
        private static extern Int32 add_numbers(Int32 number1, Int32 number2);

        static void Main(string[] args) {
            var simple_struct = get_simple_struct();
            Console.WriteLine(simple_struct.field_one);
            Console.WriteLine(simple_struct.field_two);
            Console.WriteLine(Marshal.PtrToStringAnsi(simple_struct.string_field));
            free_string();

            var addedNumbers = add_numbers(10, 5);
            Console.WriteLine(addedNumbers);
            // Console.ReadLine();
        }
    }
}