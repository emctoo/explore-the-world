#!/usr/bin/env python
# coding: utf-8

import random, time

import zmq
import zmq.decorators
import numpy as np


def send_numpy_array(socket: zmq.Socket, header, M, flags=0, copy=True, track=False):
    """"""
    # socket.send_string(header, flags | zmq.SNDMORE)
    socket.send_json({'dtype': str(M.dtype), 'shape': M.shape}, flags | zmq.SNDMORE)
    return socket.send(M.copy(), flags=flags, copy=copy, track=track)


# https://stackoverflow.com/questions/7706339/grayscale-to-red-green-blue-matlab-jet-color-scale
def grayscale_to_rgb(v, vmin=0., vmax=1.):

    if v <= vmin:
        v = vmin
    if v >= vmax:
        v = vmax

    dv = vmax - vmin

    if v < vmin + 0.25 * dv:
        return [
            0,
            4 * (v - vmin) / dv,
            1.0,
        ]

    if v < vmin + 0.5 * dv:
        return [
            0,
            1.0,
            1 + 4 * (vmin + 0.25 * dv - v) / dv,
        ]

    if v < vmin + 0.75 * dv:
        return [
            4 * (v - vmin - 0.5 * dv) / dv,
            1.0,
            0,
        ]

    return [
        1.0,
        1 + 4 * (vmin + 0.75 * dv - v) / dv,
        0,
    ]


@zmq.decorators.socket(zmq.REP)  # pylint: disable=E1101
def main(color_pub_socket: zmq.Socket = None):
    color_pub_socket.bind('tcp://0.0.0.0:1997')

    print('starting publishing ...')

    counter = 0
    scale = 1000
    while True:
        # try:
        #     time.sleep(0.1)
        # except KeyboardInterrupt:
        #     print('\rbye.')
        #     break
        counter = (counter + 1) % scale
        color = np.random.rand(8)
        # color = np.array(grayscale_to_rgb(counter / scale) + [1.0])
        # color = np.array([color['r'], color['g'], color['b'], 1.0])
        # color = np.random.rand(4)

        color_pub_socket.recv()
        send_numpy_array(color_pub_socket, 'color', color)


if __name__ == '__main__':
    main()