import random
import zmq
import numpy as np

import kivy
from kivy.config import Config

from kivy.app import App
# from kivy.core.window import Window

from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.clock import Clock

from kivy.graphics import Color, Ellipse, Line, Rectangle

from kivy.core.text import Label as CoreLabel

def grayscale_to_rgb(v, vmin=0., vmax=1.):

    if v <= vmin:
        v = vmin
    if v >= vmax:
        v = vmax

    dv = vmax - vmin

    if v < vmin + 0.25 * dv:
        return [0, 4 * (v - vmin) / dv, 1.0]

    if v < vmin + 0.5 * dv:
        return [0, 1.0, 1 + 4 * (vmin + 0.25 * dv - v) / dv]

    if v < vmin + 0.75 * dv:
        return [4 * (v - vmin - 0.5 * dv) / dv, 1.0, 0]

    return [1.0, 1 + 4 * (vmin + 0.75 * dv - v) / dv, 0]

def random_color():
    return [random.random() for _ in range(3)]


class HVObject(BoxLayout):
    def __init__(self, req_socket: zmq.Socket):
        BoxLayout.__init__(self)

        self.req_socket = req_socket

        self.channels = [
            {
                'name': 'Fpz',
                'pos': (320, 500),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'Fz',
                'pos': (320, 400),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'Cz',
                'pos': (320, 300),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'Pz',
                'pos': (320, 200),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'F3',
                'pos': (220, 400),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'F4',
                'pos': (420, 400),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'C3',
                'pos': (220, 300),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
            {
                'name': 'C4',
                'pos': (420, 300),
                'size': (20, 20),
                'point': None,
                'font_size': 20,
                'text_rectangle': None,
            },
        ]

        self.canvas.clear()

        with self.canvas:
            self.rect = Rectangle(size=self.size, pos=self.pos)
            for nth in range(8):
                self.__setup_channel(nth)

        with self.canvas.before:
            pass

        with self.canvas.after:
            pass

        self.bind(size=self._update_rect, pos=self._update_rect)

        Clock.schedule_interval(self.callevery, 0.1)

    def __setup_channel(self, nth):
        channel = self.channels[nth]

        channel['point'] = Ellipse(pos=channel['pos'], size=channel['size'])

        Color(1, 0, 0)
        label = CoreLabel(text=channel['name'], font_size=channel['font_size'])
        label.refresh()

        text_pos = (channel['pos'][0] - channel['size'][0] - 15, channel['pos'][1])
        channel['text_rectangle'] = Rectangle(size=label.texture.size, pos=text_pos, texture=label.texture)

    def __update_channel(self, nth, value):
        channel = self.channels[nth]

        Color(*grayscale_to_rgb(value))
        self.circle = Ellipse(pos=channel['pos'], size=channel['size'])

        Color(1, 0, 0)
        label = CoreLabel(text=channel['name'], font_size=channel['font_size'])
        label.refresh()

        text_pos = (channel['pos'][0] - channel['size'][0] - 15, channel['pos'][1])
        channel['text_rectangle'] = Rectangle(size=label.texture.size, pos=text_pos, texture=label.texture)

    def callevery(self, x):
        self.req_socket.send(b'request color')
        values = recv_numpy_array(self.req_socket)
        print(values)

        with self.canvas:
            for nth in range(8):
                self.__update_channel(nth, values[nth])

    def render(self):
        self.canvas.clear()

        label = CoreLabel(text="COL %d" % self.colour, font_size=20)
        label.refresh()

        self.canvas.add(Color(1, 0, 0))
        self.canvas.add(Rectangle(size=label.texture.size, pos=(100, 100), texture=label.texture))

        self.canvas.ask_update()

    def _update_rect(self, instance, value):
        self.rect.pos = instance.pos
        self.rect.size = instance.size

        # self.label.pos = instance.pos

def recv_numpy_array(socket, flags=0, copy=True, track=False):
    md = socket.recv_json(flags=flags)
    buf = socket.recv(flags, copy, track)
    return np.frombuffer(buf, dtype=md['dtype']).reshape(md['shape'])

class MyPaintApp(App):
    def build(self):
        context = zmq.Context()
        req_socket: zmq.Socket = context.socket(zmq.REQ)
        req_socket.connect('tcp://localhost:1997')

        return HVObject(req_socket)


if __name__ == '__main__':
    # Window.size = (640, 640)
    Config.set('graphics', 'width', '640')
    Config.set('graphics', 'height', '640')
    Config.set('graphics', 'resizable', False)
    MyPaintApp().run()