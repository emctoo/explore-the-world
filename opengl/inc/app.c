#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <simple2d.h>

// https://github.com/simple2d/simple2d
// clang ../src/*.c $^ -o $@ -I../include -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lm -lglfw -lGL -lglut


typedef struct point_basic {
    const int x, y;
    const char *text;
} basic_t;

const int num_basics = 8;
basic_t basics[num_basics] = {
    {.x = 318, .y = 119, .text = "Fpz"},
    {.x = 318, .y = 279, .text = "Fz"},
    {.x = 318, .y = 437, .text = "Cz"},
    {.x = 318, .y = 568, .text = "Pz"},
    {.x = 110, .y = 272, .text = "C3"},
    {.x = 527, .y = 272, .text = "C4"},
    {.x = 97, .y = 447, .text = "F3"},
    {.x = 547, .y = 447, .text = "F4"},
};

typedef struct color { GLfloat r, g, b; } color_t;

void render_background_image(void) {
  S2D_Image *image = S2D_CreateImage("./standard_1005.png");
  image->x = 0, image->y = 0;
  S2D_DrawImage(image);
  S2D_FreeImage(image);
}

void value_to_color(double value, color_t *color, double vmin, double vmax) {
  if (value < vmin) value = vmin;
  if (value > vmax) value = vmax;

  double dv = vmax - vmin;
  if (value < vmin + 0.25 * dv) {
    color->r = 0;
    color->g = 4 * (value - vmin) / dv;
    color->b = 1.0;
    return;
  }

  if (value < vmin + 0.50 * dv) {
    color->r = 0;
    color->g = 1.0;
    color->b = 1.0 + 4 * (vmin + 0.25 * dv - value) / dv;
    return;
  }

  if (value < vmin + 0.75 * dv) {
    color->r = 4 * (value - vmin - 0.5 * dv) / dv;
    color->g = 1.0;
    color->b = 0;
    return;
  }

  color->r = 1.0;
  color->g = 1 + 4 * (vmin + 0.75 * dv - value) / dv;
  color->b = 0;
}

void render_point(basic_t *basic, double value) {
  const char *font_file = "./intuitive.ttf";

  char *text = alloca(sizeof(char) * 1024);
  snprintf(text, 1024, "%s %.3f", basic->text, value);

  S2D_Text *txt = S2D_CreateText(font_file, text, 20);

  txt->x = basic->x, txt->y = basic->y + 40;
  txt->color.r = 0, txt->color.g = 0, txt->color.b = 1;
  S2D_DrawText(txt);
  S2D_FreeText(txt);

  color_t *color = alloca(sizeof(color_t) * 1);
  value_to_color(value, color, 0, 1);
  S2D_DrawCircle(basic->x, basic->y, 32, 10, color->r, color->g, color->b, 1);
}

int counter_max = 1024;
int counter = 0;
int phases[num_basics] = {0};

void render() {
  counter = (counter + 1) % counter_max;

  render_background_image();
  for (int nth = 0; nth < num_basics; ++nth) {
    render_point(basics + nth, (double)((counter + phases[nth]) % counter_max) / (double)counter_max);
  }
}

void cleanup(void) {
  printf("\rbye.\n");
}

int main() {
  for (int nth = 0; nth < num_basics; ++nth) phases[nth] = rand() % counter_max;

  S2D_Window *window = S2D_CreateWindow("阻抗", 640, 640, NULL, render, 0);
  S2D_Show(window);

  cleanup();
  return 0;
}