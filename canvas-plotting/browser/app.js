const canvas = document.querySelector('#my-canvas');
const ctx = canvas.getContext('2d');
const width = canvas.width, height = canvas.height;

// 左上角是(0, 0), 值范围的问题还需要伸缩，所以需要做坐标变换
const generate_values = (fn, width, height, npoints, { xmin, xmax, ymin, ymax }) => {
    let xscale = width / (xmax - xmin);
    let yscale = height / (ymax - ymin);

    let ys = new Array(npoints);
    for (let x = 0; x < npoints; x++) {
        let xFnVal = (x / xscale) - xmin;
        ys[x] = height - (fn(xFnVal) - ymin) * yscale;
    }

    return ys;
}

let xs = Array.from(Array(width).keys());
let ys = [];

const plotting = (ctx, width, height, values) => {
    ys = ys.concat(values)
    if (ys.length > width) ys = ys.slice(-width)

    console.log(`${xs.length}, values: ${values.length}, ys: ${ys.length}`);

    ctx.clearRect(0, 0, width, height); // 清空，准备重绘

    ctx.beginPath();

    ctx.moveTo(xs[0], ys[0]);
    for (let nth = 0; nth < width; ++nth) ctx.lineTo(xs[nth], ys[nth]);

    ctx.strokeStyle = "red", ctx.lineWidth = 0.8;
    ctx.stroke();
}

const fn = x => Math.sin(x) + Math.sin(x * 2); //  + Math.random() * 1.2;

let start = 0, interval = 50;

function redraw() {
    const pi = Math.PI;
    let ranges = { xmin: start, xmax: start + 4 * pi, ymin: -4, ymax: 4 };
    let npoints = 170;
    let values = generate_values(fn, width, height, npoints, ranges);

    plotting(ctx, width, height, values);

    start += 0.1;
}

setInterval(redraw, 50);