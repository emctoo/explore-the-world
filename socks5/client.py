#!/usr/bin/env python
# coding: utf8

# https://github.com/Anorov/PySocks
import logging
import socks

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(lineno)s - %(message)s')

s = socks.socksocket()
s.set_proxy(socks.SOCKS5, 'localhost', 8888)

s.connect(('baidu.com', 80))
s.sendall(b'GET / HTTP/1.1 \r\n\r\n')
logging.info(s.recv(1024 * 4))