#!/usr/bin/env python
# coding: utf8

# https://zhuanlan.zhihu.com/p/28645724
# https://www.ietf.org/rfc/rfc1928.txt

import socket, threading, logging, select, struct

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(lineno)s - %(message)s')


def send_data(sock, data):
    nbytes_sent = 0
    while True:
        r = sock.send(data[nbytes_sent:])
        if r < 0:
            return r
        nbytes_sent += r
        if nbytes_sent == len(data):
            return nbytes_sent


def handle_tcp(sock, remote):
    try:
        fdset = [sock, remote]
        while True:
            r, w, e = select.select(fdset, [], [])

            if sock in r:
                data = sock.recv(1024 * 4)
                if len(data) <= 0:
                    break
                result = send_data(remote, data)
                if result < len(data):
                    raise Exception('fail to send all data')

                if remote in r:
                    data = remote.recv(1024 * 4)
                    if len(data) <= 0:
                        break
                    result = send_data(sock, data)
                    if result < len(data):
                        raise Exception('fail to send all remote data')
    except Exception as ex:
        raise (e)
    finally:
        sock.close()
        remote.close()


def handle_conn(sock, addr):
    sock.recv(256)  # 不用解析了，下面直接返回无密码认证

    sock.send(b'\x05\x00')

    data = sock.recv(4) or '\x00' * 4

    # CMD
    mode = data[1]
    if mode != 1:  # 1 CONNECT
        return

    # 忽略RSV
    # ATYP
    address_type = data[3]
    if address_type == 1:  # ipv4
        # DST.ADDR
        addr_ip = sock.recv(4)
        remote_address = socket.inet_ntoa(addr_ip)
    elif address_type == 3:  # domain
        addr_len = int.from_bytes(sock.recv(1), byteorder='big')
        remote_address = sock.recv(addr_len)
    elif addr_type == 4:  # ipv6
        addr_ip = sock.recv(16)
        remote_address = socket.inet_ntop(socket.AF_INET6, addr_ip)
    else:
        return

    # DST.PORT
    remote_address_port = struct.unpack('>H', sock.recv(2))

    # 返回给客户端(6. Replies)
    bind_addr, bind_port = socket.inet_aton('0.0.0.0'), struct.pack('>H', 8888)
    reply = b'\x05\x00\x00\x01' + bind_addr + bind_port  # VER REP(succeeded, 0x00), RSV(0x00), ATYP(1, ipv4)
    sock.send(reply)

    try:
        remote = socket.create_connection((remote_address, remote_address_port[0]))
        logging.info('remote address: %s', remote)
    except socket.error as err:
        logging.error('error happens, %s', err)

    handle_tcp(sock, remote)


def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server.bind(('localhost', 8888))
    server.listen(5)

    try:
        logging.info('serving ...')

        while True:
            client, addr = server.accept()
            client_thread = threading.Thread(target=handle_conn, args=(client, addr))
            client_thread.start()
    except socket.error as err:
        logging.error('error happens, %s', err)
    except KeyboardInterrupt:
        print('\rbye.')


if __name__ == "__main__":
    main()