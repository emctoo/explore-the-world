#!/usr/bin/env python
# coding: utf8

import random, datetime
import asyncio
import json

import sanic
from sanic import response
from sanic.log import logger as logger

import sanic_session
import sanic_jinja2

import qiniu

app = sanic.Sanic()
sanic_session.Session(app)
jinja = sanic_jinja2.SanicJinja2(app)

# https://developer.qiniu.com/censor/api/5588/image-censor

QINIU_ACCESS_KEY = 'AS2tygAxphfoA5LdwxltAJiKWqqc7C-dBiPwneFe'
QINIU_SECRET_KEY = 'V1AG5Z89X43FVM9EVMzI_l5cqVFtUbsaYi3SWt0p'

QINIU_CLIENT = qiniu.Auth(QINIU_ACCESS_KEY, QINIU_SECRET_KEY)


def censor_request(uri):
    # scenes =  {
    #   'censor': ['pulp', 'terror', 'politician'],
    #   'pulp': ['pulp'],
    #   'terror': ['terror'],
    #   'politician': ['politician'],
    #   'ads': ['ads']
    # }
    url = 'http://ai.qiniuapi.com/v3/image/censor'
    auth = qiniu.QiniuMacAuth(QINIU_ACCESS_KEY, QINIU_SECRET_KEY)
    body = {
        "data": {
            "uri": uri,
        },
        "params": {
            "scenes": ['pulp', 'terror', 'politician']
        },
    }

    ret, res = qiniu.http._post_with_qiniu_mac(url, body, auth)
    print(res)

    headers = {
        "code": res.status_code,
        "reqid": res.req_id,
        "xlog": res.x_log,
    }

    print(json.dumps(headers, indent=2, ensure_ascii=False))
    print(json.dumps(ret, indent=2, ensure_ascii=False))
    return {**headers, **ret}


async def delayed_work1(seconds, name=None):
    # logger.info('[%s] started', name)

    name = name or str(random.random())[2:6]
    logger.info('[%s] delay for %s seconds', name, seconds)

    await asyncio.sleep(seconds)

    result = random.randint(0, 10)
    logger.info('[%s] result %s', name, result)
    return result


async def main():
    tasks = [
        asyncio.create_task(
            delayed_work1(
                random.randint(3, 10), name='oo-' + str(random.random())[2:6]))
        for _ in range(3)
    ]
    gathered_tasks = asyncio.gather(*tasks)
    logger.info('gathered task created: %s', gathered_tasks)

    await asyncio.sleep(3)

    await delayed_work1(10, 'main')

    print(await gathered_tasks)


@app.listener('after_server_start')
async def init_channels(app, loop):
    app.channels = {}


async def register_channel(app, channel_name, name, ws):
    if channel_name not in app.channels:
        app.channels[channel_name] = {}
    app.channels[channel_name][name] = ws


@app.websocket('/feed')
async def feed(req, ws):
    await register_channel(req.app, 'group:systm', 'hello', ws)
    while True:
        data = await ws.recv()
        print('Received: ' + data)


@app.route('/ep/1', ['POST'])
async def dispatch(req):
    return response.text('done')


@app.route('/upload-photo', ['POST'])
async def upload_photo(req):
    photos = req.files.getlist('file')
    if len(photos) != 1:
        logger.info('一次只能上传一张图片, %s', len(photos))
        return response.text('one file only')

    logger.info('form: %s', req.form)

    photo = photos[0]
    now = datetime.datetime.now().strftime('%F.%T')
    target_path = f'/tmp/uploaded/{now}+{photo.name}'
    logger.info('uploading photo, name: %s, type: %s => %s', photo.name,
                photo.type, target_path)

    with open(target_path, 'wb') as output_file:
        output_file.write(photo.body)

    return response.text('done')


app.static('/', './index.html')

if __name__ == '__main__':
    # asyncio.get_event_loop().run_until_complete(main())
    app.run(debug=True)
