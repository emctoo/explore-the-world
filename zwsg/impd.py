#!/usr/bin/env python
# coding=utf-8

import numpy as np
import zmq
import zmq.decorators
import logging, time, random, sys

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(lineno)s - %(message)s')


@zmq.decorators.context()
def impd(context: zmq.Context):
    control_socket: zmq.Socket = context.socket(zmq.REP)
    control_socket.bind('tcp://192.168.1.42:2019')

    pub_socket: zmq.Socket = context.socket(zmq.PUB)
    pub_socket.bind('tcp://192.168.1.42:2018')

    poller: zmq.Poller = zmq.Poller()
    poller.register(control_socket)

    device_status = 'connected'
    logging.info('impd server running ...')

    sys.stdout.write('\r' + device_status + ' ')
    sys.stdout.write('-')
    sys.stdout.flush()

    counter = 0

    while True:
        counter = (counter + 1) % 4
        sys.stdout.write('\r' + device_status + ' ' + ['-', '\\', '|', '/'][counter])
        sys.stdout.flush()

        events = dict(poller.poll(100))
        if events.get(control_socket, False):
            control_command = control_socket.recv_string()

            print('\r')
            logging.info('device status updated: [%s]', control_command)

            if control_command == 'start-impedance':
                device_status = 'impedance-started'
                sys.stdout.write('\r' + device_status + ' ')

                control_socket.send_string('ok')

            elif control_command == 'stop-impedance':
                device_status = 'connected'
                sys.stdout.write('\r' + device_status + ' ')

                pub_socket.send_string('bye')
                control_socket.send_string('ok')

            elif control_command == 'start-sampling':
                device_status = 'sampling-started'
                sys.stdout.write('\r' + device_status + ' ')
                control_socket.send_string('ok')

            elif control_command == 'stop-sampling':
                device_status = 'connected'
                sys.stdout.write('\r' + device_status + ' ')
                control_socket.send_string('ok')

            elif control_command == 'status':
                control_socket.send_string(device_status)

        if device_status == 'impedance-started':
            payload = [random.randint(0, 256) for _ in range(10)]
            pub_socket.send_json(payload)


if __name__ == '__main__':
    impd()