#!/usr/bin/env python
# coding=utf-8

import socketserver
import hashlib
import base64
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(lineno)s - %(message)s')


def websocket_accept(key):
    MAGIC = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
    return base64.b64encode(hashlib.sha1((key + MAGIC).encode()).digest()).decode()


class MyTCPHandler(socketserver.BaseRequestHandler):
    __400_BAD_REQUEST = "HTTP/1.1 400 Bad Request\r\n" + \
                        "Content-Type: text/plain\r\n" + \
                        "Connection: close\r\n" + \
                        "\r\n" + \
                        "Incorrect request"

    __101_SWITCHING_PROTOCOLS_TEMPLATE = ("HTTP/1.1 101 Switching Protocols\r\n"
                                          "Upgrade: websocket\r\n"
                                          "Connection: Upgrade\r\n"
                                          "Sec-WebSocket-Accept: {response_key}\r\n\r\n")

    def handle(self):
        self.data = self.request.recv(1024).strip().decode('utf-8')

        if not ("Connection: Upgrade" in self.data and "Upgrade: websocket" in self.data):
            logging.info('no websocket connection, 400 error')
            self.request.sendall(MyTCPHandler.__400_BAD_REQUEST.encode())
            return

        headers = self.data.split("\r\n")
        for h in headers:
            if "Sec-WebSocket-Key" in h:
                key = h.split(" ")[1]

        logging.info('websocket key: %s', key)
        self.__shake_hand(key)

        logging.info('new websocket client, blocking here ...')
        while True:
            payload = self.__decode_frame(self.request.recv(1024).strip())
            self.__send_frame(payload)

            decoded_payload = payload.decode('utf-8')
            if "bye" == decoded_payload.lower():
                logging.info('websocket client leaves ...')
                return

        logging.info('single handle finish')

    def __shake_hand(self, key):
        resp = MyTCPHandler.__101_SWITCHING_PROTOCOLS_TEMPLATE.format(**{'response_key': websocket_accept(key)})

        logging.info('handshake response')
        self.request.sendall(resp.encode('utf-8'))

    def __decode_frame(self, frame: bytes):
        # RFC6455 5.2 Base Framing Protocol
        logging.info('frame (%s): [%s]', type(frame), frame)

        # assuming it's masked, hence removing the mask bit(MSB) to get len. also assuming len is <125
        payload_len = frame[1] - 128

        mask = frame[2:6]
        encrypted_payload = frame[6:6 + payload_len]

        # client -> server 是掩码处理的
        return bytearray([encrypted_payload[i] ^ mask[i % 4] for i in range(payload_len)])

    def __send_frame(self, payload):
        frame = [129]  # setting fin to 1 and opcpde to 0x1
        frame += [len(payload)]  # adding len. no masking hence not doing +128
        frame_to_send = bytearray(frame) + payload  # adding payload
        self.request.sendall(frame_to_send)


if __name__ == "__main__":
    socketserver.TCPServer.allow_reuse_address = True

    HOST, PORT = "0.0.0.0", 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    logging.info('listening on %s:%s ...', HOST, PORT)
    server.serve_forever()