#!/usr/bin/env python
# coding=utf-8

import asyncio
import websockets
import zmq
import zmq.asyncio
import logging
import threading
import struct, json

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(lineno)s - %(message)s')

impedance_url = 'tcp://127.0.0.1:5556'


def impedanceWorker(websocket, isStopped: threading.Event):
    logging.info('impedance thread isStopped: %s', isStopped.is_set())
    logging.info('impedance client connected: [%s]', id(websocket))

    context: zmq.asyncio.Context = zmq.Context.instance()

    sub_socket = context.socket(zmq.SUB)  # pylint: disable=E1101
    sub_socket.connect(impedance_url)
    sub_socket.setsockopt(zmq.SUBSCRIBE, b'')  # pylint: disable=E1101

    logging.info('[impedanceThread] running ...')
    while isStopped.is_set() is False:
        # logging.info('.')
        try:
            websocket.send(sub_socket.recv_string())
        except websockets.exceptions.ConnectionClosed:
            logging.info('ws connection closed')
            break

    logging.info('[impedanceThread] ends.')


async def impedanceTask(websocket):
    logging.info('impedance client connected: [%s]', id(websocket))

    context: zmq.asyncio.Context = zmq.Context.instance()

    sub_socket = context.socket(zmq.SUB)  # pylint: disable=E1101
    sub_socket.connect(impedance_url)
    sub_socket.setsockopt(zmq.SUBSCRIBE, b'')  # pylint: disable=E1101

    poller = zmq.Poller()
    poller.register(sub_socket, zmq.POLLIN)

    while True:
        try:
            events = dict(poller.poll(5))
            if events.get(sub_socket, False):
                # logging.info('warting for impedance payload')
                impedancePayloadBuf = sub_socket.recv()
                # logging.info('first frame payload: [%s]', impedancePayloadBuf)

                # impedancePayload = impedancePayloadBuf.decode('utf-8')
                if impedancePayloadBuf == b'bye':
                    break

                # logging.info('forward impedance data to websocket')
                if impedancePayloadBuf == b'data':
                    impedancePayloadBuf = sub_socket.recv()
                    # logging.info('buf size: %s', len(impedancePayloadBuf))

                    values = struct.unpack('d' * 10, impedancePayloadBuf)
                    # logging.info('values: %s', values)

                    await websocket.send(json.dumps(values))
        except websockets.exceptions.ConnectionClosed:
            logging.info('ws connection closed')
            break

    logging.info('impedanceTask ends.')


async def mainTask(websocket, path):
    logging.info('[main] path: %s', path)

    await impedanceTask(websocket)
    # isStopped = threading.Event()
    # logging.info('[main] impedance thread isStopped: %s', isStopped.is_set())
    # impedanceThread = threading.Thread(name='impedance-forward', target=impedanceWorker, args=(websocket, isStopped))
    # impedanceThread.start()


def main(host='0.0.0.0', port=9999):
    logging.info('listening on %s:%s ...', host, port)

    start_server = websockets.serve(mainTask, host, port)
    asyncio.get_event_loop().run_until_complete(start_server)

    asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    main()
