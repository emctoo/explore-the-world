#!/usr/bin/env python
"""
synopsis:
    Weather update server
    Binds PUB socket to tcp://*:5556
    Publishes random weather updates
    Modified for async/ioloop: Dave Kuhlman <dkuhlman(at)davekuhlman(dot)org>
usage:
    python wuserver.py
"""

import sys
import random
import zmq
from zmq.asyncio import Context, ZMQEventLoop
import asyncio, logging

CLIENT_ADDRESS = "tcp://*:5557"

logging.basicConfig(
    level=logging.INFO, format='%(asctime)s - %(lineno)s - %(message)s')


async def run_server(context):
    socket = context.socket(zmq.PUB)
    socket.bind(CLIENT_ADDRESS)

    logging.info('serving ...')
    while True:
        choices = [
            'start-impedance',
            'stop-impedance',
            'start-sampling',
            'stop-sampling',
            'stop-application',
        ]
        payload = random.choice(choices)
        payload = random.choices(choices, [0.2, 0.2, 0.2, 0.2, 0.01], k=1)[0]

        logging.info('sending command [%s]', payload)
        await socket.send_string(payload)

        await asyncio.sleep(random.randint(1, 4))


async def run(loop):
    context = Context()
    while True:
        await run_server(context)


def main():
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run(loop))
    except KeyboardInterrupt:
        print('\nFinished (interrupted)')


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
