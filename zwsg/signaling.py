#!/usr/bin/env python
# coding=utf-8

import asyncio
import websockets
import zmq
import zmq.asyncio
import logging
import threading

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(lineno)s - %(message)s')

impedance_url = 'tcp://127.0.0.1:2018'
device_control_url = 'tcp://127.0.0.1:1989'


async def signalingTask(websocket):
    logging.info('signaling client connected: [%s]', id(websocket))

    context = zmq.Context.instance()

    device_control_socket: zmq.Socket = context.socket(zmq.REQ)  # pylint: disable=E1101
    device_control_socket.connect(device_control_url)

    while True:
        logging.info('waiting for signaling message ...')
        payload = await websocket.recv()
        logging.info('signaling task payload: [%s]', payload)

        if payload == 'start-impedance':
            device_control_socket.send_string('start-impedance')
            logging.info('device response: [%s]', device_control_socket.recv_string())

            await websocket.send('impedance-started')

        if payload == 'stop-impedance':
            device_control_socket.send_string('stop-impedance')
            logging.info('device response: [%s]', device_control_socket.recv_string())

            await websocket.send('impedance-stopped')

        if payload == 'start-sampling':
            device_control_socket.send_string('start-sampling')
            logging.info('device response: [%s]', device_control_socket.recv_string())

            await websocket.send('sampling-started')

        if payload == 'stop-sampling':
            device_control_socket.send_string('stop-sampling')
            logging.info('device response: [%s]', device_control_socket.recv_string())

            await websocket.send('sampling-stopped')

        if payload == 'device-status':
            device_control_socket.send_string('status')
            device_status = device_control_socket.recv_string()
            logging.info('device response: [%s]', device_status)

            await websocket.send(f'device-status-update {device_status}')


async def mainTask(websocket, path):
    logging.info('[main] path: %s', path)
    await signalingTask(websocket)


def main(host='0.0.0.0', port=9998):
    logging.info('listening on %s:%s ...', host, port)

    start_server = websockets.serve(mainTask, host, port)
    asyncio.get_event_loop().run_until_complete(start_server)

    asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    main()
