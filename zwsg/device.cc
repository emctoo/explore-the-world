#include <iostream>
#include <random>
#include <thread>

#include <time.h>
#include <zmq.hpp>
#include <cstring>

void working(zmq::context_t *context, std::mt19937 gen, std::uniform_real_distribution<> dis) {
    double values[10] = {0};

    zmq::socket_t publisher (*context, ZMQ_PUB);
    publisher.bind("tcp://*:5556");

    zmq::socket_t thread_control_socket(*context, ZMQ_PAIR);
    thread_control_socket.connect("inproc://tmp/device-control");
    thread_control_socket.send("impedance-started", 17);

    zmq::pollitem_t items [] = {
        { thread_control_socket, 0, ZMQ_POLLIN, 0 }
    };

    printf("                                                                           [impedance thread] start publishing impedance values ...\n");
    while (true) {
        zmq::poll(items, 1, std::chrono::milliseconds(100));
        if (items[0].revents & ZMQ_POLLIN) {
            zmq::message_t message;
            thread_control_socket.recv(&message, 0);
            printf("                                                                           [impedance thread] thread 控制命令: [%.*s]\n", message.size(), message.data());

            const char *stop_command = "stop";
            if (std::memcmp(message.data(), "stop", strlen("stop")) == 0) break;
        }

        for (int n = 0; n < 10; ++n) values[n] = dis(gen);

        const char *data_type = "data";
        publisher.send(data_type, strlen(data_type), ZMQ_MORE);
        publisher.send((void *)values, sizeof(values));
    }

    publisher.send("bye", 3);
    thread_control_socket.send("impedance-stopped", 17);

    publisher.close();
    thread_control_socket.close();
    printf("                                                                           [impedance thread] exit\n");
}

int main() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 256);

    zmq::context_t context(1);

    zmq::socket_t control_socket(context, ZMQ_SUB);
    control_socket.connect("tcp://compaq:5557");
    control_socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);

    zmq::socket_t thread_control_socket(context, ZMQ_PAIR);
    thread_control_socket.bind("inproc://tmp/device-control");

    zmq::pollitem_t items [] = {
        {thread_control_socket, 0, ZMQ_POLLIN, 0},
        {control_socket, 0, ZMQ_POLLIN, 0 }
    };

    // disconnected, connected, impedance-started, sampling-started
    std::string device_status = "connected";
    zmq::message_t message;

    while (true) {
        zmq::poll(items, 2, std::chrono::milliseconds(10));

        if ((items[0].revents & ZMQ_POLLIN)) {
            thread_control_socket.recv(&message, 0);
            printf("                          [main::thread-control] thread response: [%.*s]\n", message.size(), message.data());

            if (std::memcmp(message.data(), "sampling-started", strlen("sampling-started")) == 0) device_status = "sampling-started";
            if (std::memcmp(message.data(), "sampling-stopped", strlen("sampling-stopped")) == 0) device_status = "connected";
            if (std::memcmp(message.data(), "impedance-started", strlen("impedance-started")) == 0) device_status = "impedance-started";
            if (std::memcmp(message.data(), "impedance-stopped", strlen("impedance-stopped")) == 0) device_status = "connected";
        }

        if ((items[1].revents & ZMQ_POLLIN)) {
            control_socket.recv(&message, 0);

            printf("[main::control] [%s] => [%.*s], ", device_status.c_str(), message.size(), message.data());

            if (std::memcmp(message.data(), "start-sampling", strlen("start-sampling")) == 0) {
                if (device_status != "connected") {
                    printf("no\n");
                    continue;
                }

                printf("ok\n");

                device_status = "sampling-started";
                continue;
            }

            if (std::memcmp(message.data(), "stop-sampling", strlen("stop-sampling")) == 0) {
                if (device_status != "sampling-started") {
                    printf("no\n");
                    continue;
                }

                printf("ok\n");

                device_status = "connected";
                continue;
            }


            if (std::memcmp(message.data(), "start-impedance", strlen("start-impedance")) == 0) {
                if (device_status != "connected") {
                    printf("no\n");
                    continue;
                }

                printf("ok\n");
                printf("[main::control] start impedance thread now ...\n");
                std::thread t(working, &context, gen, dis);
                t.detach();

                continue;
            }

            if (std::memcmp(message.data(), "stop-impedance", strlen("stop-impedance")) == 0) {
                if (device_status != "impedance-started") {
                    printf("no\n");
                    continue;
                }

                printf("ok\n");

                printf("[main::control] stop impedance thread now ...\n");
                thread_control_socket.send("stop", 4);

                continue;
            }

            if (std::memcmp(message.data(), "stop-application", strlen("stop-application")) == 0) {
                printf("[main::control] quiting command, device status: %s\n", device_status.c_str());

                // TODO: 是否需要结束进程
                // printf("[main::control] check thread now ...\n");
                // if (device_status == "impedance-started" || device_status == "sampling-started") {
                //     printf("[main::control] send [stop] to thread ...\n");
                //     thread_control_socket.send("stop", 4);
                // }

                printf("[main::control] breaking now ...\n");
                break;
            }
        }
    }

    control_socket.close();
    thread_control_socket.close();
    printf("bye.\n");
    return 0;
}