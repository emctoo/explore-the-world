#!/usr/bin/env python

"""
synopsis:
    Weather update client
    Connects SUB socket to tcp://localhost:5556
    Collects weather updates and finds avg temp in zipcode
    Modified for async/ioloop: Dave Kuhlman <dkuhlman(at)davekuhlman(dot)org>
usage:
    python wuclient.py [zip_code]
notes:
    Include an optional zip code in the range 10000 - 10004
"""

import sys
import zmq
from zmq.asyncio import Context, ZMQEventLoop
import asyncio
import logging
import struct

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(lineno)s - %(message)s')

SERVER_ADDRESS = "tcp://127.0.0.1:5556"


async def run_client(context):
    #  Socket to talk to server
    socket = context.socket(zmq.SUB)
    socket.connect(SERVER_ADDRESS)
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    print("Collecting updates from weather server...")

    while True:
        command = await socket.recv()
        logging.info('command: [%s]', command)

        if command == b'data':
            payload = await socket.recv()
            logging.info('data size: %s', len(payload))

            output_data = struct.unpack('d' * 10, payload)
            logging.info('payload: [%s]', output_data)

        if command == b'bye':
            logging.info('client leaves.')


async def run(loop):
    context = Context()
    await run_client(context)


def main():
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(run(loop))
    except KeyboardInterrupt:
        print('\nFinished (interrupted)')


if __name__ == '__main__':
    main()
