#include <iostream>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <czmq.h>

int parse(const char *json) {
  // 1. Parse a JSON string into DOM.
  // const char *json = "{\"project\":\"rapidjson\",\"stars\":10}";
  rapidjson::Document d;
  d.Parse(json);

  rapidjson::Value &type = d["type"], &command = d["command"];
  std::cout << "type: " << type.GetString() << ", command: " << command.GetString() << '\n';
  // 2. Modify it by DOM.
  // rapidjson::Value &s = d["stars"];
  // s.SetInt(s.GetInt() + 1);

  // 3. Stringify the DOM
  // rapidjson::StringBuffer buffer;
  // rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  // d.Accept(writer);

  // Output {"project":"rapidjson","stars":11}
  // std::cout << buffer.GetString() << std::endl;
  return 0;
}

int main() {
  zsock_t *sub = zsock_new_sub("ipc:///tmp/control-pub.ipc", "");
  while (true) {
    char *buf = zstr_recv(sub);
    int status = 0;
    // int status = zsock_recv(sub, "s", &buf);
    printf("status: %d, received message: [%s]\n", status, buf);
    parse(buf);
    zstr_free(&buf);
  }

  zsock_destroy(&sub);
}