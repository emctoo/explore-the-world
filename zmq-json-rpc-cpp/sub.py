#!/usr/bin/env python
# coding=utf-8

import sys
import zmq.decorators

@zmq.decorators.socket(zmq.SUB)
def control_sub_things(sock: zmq.Socket = None):
    sock.connect('ipc:///tmp/control-pub.ipc')
    sock.setsockopt_string(zmq.SUBSCRIBE, '')

    # print('running ...')
    while True:
        payloads = sock.recv_string()
        sys.stdout.write(payloads + '\n')
        sys.stdout.flush()


def main():
    control_sub_things()

if __name__ == '__main__':
    main()