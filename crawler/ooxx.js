const puppeteer = require('puppeteer');
const axios = require('axios');
const fs = require('fs');

let currentNumber = 1;

async function run(url) {
  console.log('starting ...');

  const args = ['--no-sandbox', '--disable-setuid-sandbox']
  const headless = false;
  const executablePath = '/usr/bin/google-chrome-unstable'
  const browser = await puppeteer.launch({ args, headless, executablePath, startingUrl: url });


  const page = await browser.newPage();

  console.log(`visit url ${url} ...`)
  await page.goto(url, {
    waitUntil: 'networkidle2',
    timeout: 3000000
  });

  console.log(`fetch image urls ...`);
  let imageUrls = await page.evaluate(() => {
    const elements = [...document.querySelectorAll('.commentlist img')]
    console.dir(elements);

    return elements.map(el => el.src);
  });

  console.log(`image urls`, imageUrls);
  axios.post('http://localhost:2000', imageUrls)
  imageUrls.forEach((e, i) => {
    console.log(e)
    if (currentNumber === 222) {
      browser.close();
      console.log('All pictures downloaded complete!')
      return
    }

    axios
      .get(e, { responseType: 'stream' })
      .then(res => {
        res.data.pipe(fs.createWriteStream(`./ooxx/${currentNumber}.${e.substr(e.length - 3)}`));
        currentNumber++;
      })
  });

  let nextPage = await page.evaluate(() => {
    const pageDiv = [...document.querySelectorAll('.cp-pagenavi')][0];
    const counter = pageDiv.children[1].childNodes[0].textContent.trim();
    return `http://jandan.net/ooxx/page-${counter}`;
  })

  console.log('submit image urls ...');


  setTimeout(function () {
    run(nextPage)
  }, 3000);
  // browser.close()
}

run('http://jandan.net/ooxx');