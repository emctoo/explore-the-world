#!/usr/bin/env fish
#

# -C, --cvs-exclude

rsync -avz --delete --cvs-exclude \
  --exclude .mypy_cache --exclude sync.fish --exclude .gitignore \
  ./ closer-prod:~/closer-web/
