#!/usr/bin/env python
# coding: utf8

import os
import socks
import telethon
import asyncio
import logging
import random
from lxml import etree
import requests
import aiohttp

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

proxy = (socks.SOCKS5, '127.0.0.1', 1080)

api_id = '735496'
api_hash = '25ce01aef31c5e0a5bf83db1bfd0eeed'




class Downloader:
  catalog_list = ['xinggan', 'qingchun', 'xiaohua', 'chemo', 'qipao', 'mingxing']

  def __init__(self):
    self.output_path = '/tmp/f.jpg'
    pass

  def __ref_set(self, src, catalog):
    ref = src[25:-4].split('/')
    if src[-5:-4] == 1:
      return f'http://www.mm131.com/{catalog}/' + ref[0] + '.html'
    return f'http://www.mm131.com/{catalog}/' + ref[0] + '_' + ref[1] + '.html'


  def __set_header(self, referer, catalog):
    return {
      'Pragma': 'no-cache',
      'Accept-Encoding': 'gzip, deflate',
      'Accept-Language': 'zh-CN,zh;q=0.9,ja;q=0.8,en;q=0.7',
      'Cache-Control': 'no-cache',
      'Connection': 'keep-alive',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',
      'Accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
      'Referer': self.__ref_set(referer, catalog),
    }


  def listUrls(self, catalog):
    url = f'http://www.mm131.com/{catalog}/'
    content = etree.HTML(requests.get(url).text)
    href = content.xpath('//dl[@class="list-left public-box"]//dd[1]/a/@href')[0]
    # logging.info('hrefs: %s', href)

    latest_id = int(href[-9:-5])  # http://www.mm131.com/xinggan/4874.html
    # logging.info('newid: %s', latest_id)

    i = random.randint(1, latest_id + 1)
    urls = [f'http://img1.mm131.me/pic/{i}/{j}.jpg' for j in range(1, 60)]
    # logging.info('urls: %s', urls)
    return urls


  async def download(self, telegram_client):
    if os.path.exists(self.output_path):
      os.remove(self.output_path)

    catalog = random.choice(Downloader.catalog_list)

    urls = self.listUrls(catalog)
    url = random.choice(urls)

    async with aiohttp.ClientSession() as session:
      response = await session.get(url, headers=self.__set_header(url, catalog))
      pic = await response.read()

    if response.status == 404:
      return '404 not found!'

    # logging.info('image: %s, status code: %s', url, response.status)

    with open(self.output_path, 'wb') as pp:
      pp.write(pic)

    logging.info('%s downloaded.', url)
    await telegram_client.send_file('self', self.output_path)
    return url


async def main():
  logging.info('starting ...')

  telegram_client = telethon.TelegramClient('session_name', api_id, api_hash, proxy=proxy)

  await telegram_client.connect()
  logging.info('connected')

  authorized = await telegram_client.is_user_authorized()

  phone = '+8617380538161'
  if not authorized:
    await telegram_client.send_code_request(phone)
    code = input('code: ').strip()
    logging.info('login code: [%s]', code)
    await telegram_client.sign_in(phone, code)

  me = await telegram_client.get_me()

  dl = Downloader()
  while True:
    await dl.download(telegram_client)
    await asyncio.sleep(10)


if __name__ == '__main__':
  try:
    asyncio.run(main())
  except KeyboardInterrupt:
    print('\rbye.')
