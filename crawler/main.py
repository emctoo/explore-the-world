#!/usr/bin/env python
# coding: utf8

import os, time, asyncio
from aiomultiprocess import Pool
import aiohttp
import requests
import logging

from lxml import etree
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def ref_set(src):
  ref = src[25:-4].split('/')
  if src[-5:-4] == 1:
    return 'http://www.mm131.com/xinggan/' + ref[0] + '.html'
  return 'http://www.mm131.com/xinggan/' + ref[0] + '_' + ref[1] + '.html'


def set_header(referer):
  headers = {
    'Pragma': 'no-cache',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9,ja;q=0.8,en;q=0.7',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36',
    'Accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
    'Referer': '{}'.format(ref_set(referer)),
  }
  return headers


class AioMm131(object):
  def __init__(self, target_dir):
    self.mm_folder = target_dir
    self.each_limit = 30

  async def async_get(self, url):
    i = url[24:29]
    j = url[30:-4]

    # if not os.path.exists(self.mm_folder + i):
    #   os.makedirs(self.mm_folder + i)
    async with aiohttp.ClientSession() as session:
      logging.info('waiting for %s', url)
      response = await session.get(url, headers=set_header(url))
      # pic = await response.read()
    if response.status == 404:
      return '404 not found!'

    logging.info('image: %s, status code: %s', url, response.status)

    # with open(self.mm_folder + '%s/%s.jpg' % (i, j), 'wb') as pp:
    #   pp.write(pic)

  async def make_url(self, sta, end, limit):
    urls = [f'http://img1.mm131.me/pic/{i}/{j}.jpg' for i in range(sta, end) for j in range(1, limit)]
    return await Pool().map(self.async_get, urls)

  def go_start(self, begin, end):
    task = asyncio.ensure_future(self.make_url(begin, end, self.each_limit))
    loop: asyncio.BaseEventLoop = asyncio.get_event_loop()
    loop.run_until_complete(task)


def getmmdir():
  if os.path.exists(mm_folder) and len(os.listdir(mm_folder)) > 0:
    return int(max(os.listdir(mm_folder)))
  else:
    return default_start


def listUrls():
  url = 'http://www.mm131.com/xinggan/'
  content = etree.HTML(requests.get(url).text)
  href = content.xpath('//dl[@class="list-left public-box"]//dd[1]/a/@href')[0]
  logging.info('hrefs: %s', href)

  newid = href[-9:-5]  # http://www.mm131.com/xinggan/4874.html
  logging.info('newid: %s', newid)

  urls = [f'http://img1.mm131.me/pic/{newid}/{j}.jpg' for j in range(1, 60)]
  logging.info(urls)


if __name__ == '__main__':
  # mm_folder = os.path.join(os.path.expanduser('~'), 'images/mm131/')

  listUrls()