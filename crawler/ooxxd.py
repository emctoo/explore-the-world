#!/usr/bin/env python
# coding: utf8

import sanic
from sanic import response

import logging
import json

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

app = sanic.Sanic(__name__)


@app.route('/', methods=['POST'])
async def persist_links(req):
    with open('links.json', 'r') as handle:
        links = json.load(handle)

    links += req.json

    with open('links.json', 'w') as handle:
        json.dump(links, handle, indent=2)

    return response.text('link persisted')


app.run(host='localhost', port=2000, debug=True)