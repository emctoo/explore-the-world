#!/usr/bin/env python
# coding: utf8

import requests
import logging

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s')

from telegram.ext import Updater, CallbackContext, CommandHandler
from telegram.ext import MessageHandler, Filters
updater = Updater(
  token='775292369:AAF8zIBVzVgWOpoepABlfqonY3eqnzAVaXg', use_context=True)


def start(update, context: CallbackContext):
  context.bot.send_message(
    chat_id=update.message.chat_id, text="I'm a bot, shut up please!")


start_handler = CommandHandler('start', start)
updater.dispatcher.add_handler(start_handler)


def echo(update, context: CallbackContext):
  logging.info('receving message: [%s]', update.message.text)
  context.bot.send_message(
    chat_id=update.message.chat_id, text=update.message.text)


updater.dispatcher.add_handler(MessageHandler(Filters.text, echo))

try:
  logging.info('start polling now ...')
  updater.start_polling()
except KeyboardInterrupt:
  print('\rbye.')
