// https://thispointer.com/c11-how-to-use-stdthread-as-a-member-variable-in-class/

#include <assert.h>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>

#include <thread>
#include <vector>

#include <spdlog/spdlog.h>

#include <spdlog/sinks/stdout_color_sinks.h>

using namespace std::chrono_literals; // h, min, s, ms, us, ns
// auto logger = spdlog::basic_logger_mt("c++11-thread", "/tmp/c++11-thread.log");
auto logger = spdlog::stdout_color_mt("member-variable");

class ThreadWrapper {
private:
  std::thread threadHandler;

public:
  // Delete the copy constructor
  ThreadWrapper(const ThreadWrapper &) = delete;

  // Delete the Assignment opeartor
  ThreadWrapper &operator=(const ThreadWrapper &) = delete;

  // Parameterized Constructor
  ThreadWrapper(std::function<void()> func);

  // Move Constructor
  ThreadWrapper(ThreadWrapper &&obj);

  // Move Assignment Operator
  ThreadWrapper &operator=(ThreadWrapper &&obj);

  // Destructor
  ~ThreadWrapper();
};

// Parameterized Constructor
ThreadWrapper::ThreadWrapper(std::function<void()> func) : threadHandler(func) {}

// Move Constructor
ThreadWrapper::ThreadWrapper(ThreadWrapper &&obj) : threadHandler(std::move(obj.threadHandler)) { logger->info("move Constructor is called"); }

// Move Assignment Operator
ThreadWrapper &ThreadWrapper::operator=(ThreadWrapper &&obj) {
  logger->critical("move assignment is called");
  if (threadHandler.joinable()) threadHandler.join();
  threadHandler = std::move(obj.threadHandler);
  return *this;
}

// Destructor
ThreadWrapper::~ThreadWrapper() {
  if (threadHandler.joinable()) threadHandler.join();
}

int main() {
  // default info: [%Y-%m-%d %H:%M:%S.%e] [%n] [%l] %v
  logger->set_pattern("[%Y-%m-%d %H:%M:%S.%f] [%n] [%l] [%t] %v");
  // std::function object
  std::function<void()> func = []() {
    std::this_thread::sleep_for(1s);
    logger->info("exit.");
    // logger->info("From Thread ID: {0}", std::this_thread::get_id());
  };

  {
    logger->info("创建对象，内部启动线程");
    ThreadWrapper wrapper(func);

    logger->info("出了作用域后会触发destructor, 会join thread object");
  }

  logger->info("creating vector of threads now ...");
  std::vector<ThreadWrapper> vecOfThreads;

  logger->info("add thread1");
  ThreadWrapper thwp1(func);
  vecOfThreads.push_back(std::move(thwp1));

  logger->info("add thread2");
  ThreadWrapper thwp2(func);
  vecOfThreads.push_back(std::move(thwp2));

  ThreadWrapper thwp3(func);
  logger->info("change vec[1]");
  vecOfThreads[1] = std::move(thwp3); // 修改

  logger->info("vector出作用域，vector destructor触发，ThreadWrapper desctructor 触发，join thread object");
  return 0;
}
