#include <condition_variable>
#include <functional>
#include <iostream>
#include <mutex>
#include <spdlog/spdlog.h>
#include <thread>

#include <spdlog/sinks/stdout_color_sinks.h>
using namespace std::placeholders;

using namespace std::chrono_literals; // h, min, s, ms, us, ns
auto logger = spdlog::stdout_color_mt("c++11-thread");

class Application {
  std::mutex m_mutex;
  std::condition_variable m_condVar;
  bool m_bDataLoaded;

public:
  Application() { m_bDataLoaded = false; }
  void loadData() {
    // Make This Thread sleep for 1 Second
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    logger->info("Loading Data from XML");
    // Lock The Data structure
    std::lock_guard<std::mutex> guard(m_mutex);
    // Set the flag to true, means data is loaded
    m_bDataLoaded = true;
    // Notify the condition variable
    m_condVar.notify_one();
  }

  bool isDataLoaded() const { return m_bDataLoaded; }

  void mainTask() {
    logger->info("Do Some Handshaking");

    // Acquire the lock
    std::unique_lock<std::mutex> mlock(m_mutex);

    // Start waiting for the Condition Variable to get signaled
    // Wait() will internally release the lock and make the thread to block
    // As soon as condition variable get signaled, resume the thread and
    // again acquire the lock. Then check if condition is met or not
    // If condition is met then continue else again go in wait.
    m_condVar.wait(mlock, std::bind(&Application::isDataLoaded, this));

    logger->info("Do Processing On loaded Data");
  }
};

int main() {
  logger->set_pattern("[%Y-%m-%d %H:%M:%S.%f] [%n] [%l] [%t] %v");

  Application app;

  std::thread thread_1(&Application::mainTask, &app);
  std::thread thread_2(&Application::loadData, &app);

  thread_2.join();
  thread_1.join();
  return 0;
}