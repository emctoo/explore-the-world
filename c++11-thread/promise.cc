// https://thispointer.com/c11-how-to-stop-or-terminate-a-thread/

#include <assert.h>

#include <chrono>
#include <future>
#include <iostream>
#include <spdlog/spdlog.h>

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

using namespace std::chrono_literals; // h, min, s, ms, us, ns
// auto logger = spdlog::basic_logger_mt("c++11-thread", "/tmp/c++11-thread.log");
auto logger = spdlog::stdout_color_mt("c++11-thread");

void threadFunction(std::future<void> futureObj) {
  logger->critical("Thread Start");

  while (futureObj.wait_for(1us) == std::future_status::timeout) {
    logger->critical("Doing Some Work, ease ...");
    std::this_thread::sleep_for(100ms);
  }

  logger->critical("bye");
}

int main() {

  // spdlog::set_pattern("[%F.%T] [thread %t] %v"); // strftime
  // spdlog::flush_every(1s);

  logger->info("\n");

  // Create a std::promise object
  std::promise<void> exitSignal;

  // Fetch std::future object associated with promise
  std::future<void> futureObj = exitSignal.get_future();

  // Starting Thread & move the future object in lambda function by reference
  std::thread th(threadFunction, std::move(futureObj));

  logger->info("sleep for 10 seconds");

  for (int nth = 0; nth < 1000; nth++) {
    logger->info("another 10ms");
    std::this_thread::sleep_for(10ms);
  }

  logger->info("Asking Thread to Stop ...");

  // Set the value in promise
  exitSignal.set_value();

  // Wait for thread to join
  th.join();

  logger->info("bye.");
  return 0;
}