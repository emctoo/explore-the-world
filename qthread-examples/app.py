#!/usr/bin/python3

import sys, os, time
import tempfile
import subprocess
from PyQt5 import uic, QtWidgets, QtCore

MainWindowUi, MainWindowBase = uic.loadUiType('mainwindow.ui')


class WorkerThread(QtCore.QThread):
    signal = QtCore.pyqtSignal('PyQt_PyObject')

    def __init__(self):
        QtCore.QThread.__init__(self)
        self.git_url = ""

    def run(self):
        """启动线程运行"""
        self.signal.emit('thread started.\n')

        while not self.isInterruptionRequested():
            self.signal.emit('%s working ...\n' % time.strftime('%F.%T'))
            time.sleep(2)

        self.signal.emit('thread exit.')
        print('exit')


class ExampleApp(MainWindowBase, MainWindowUi):

    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)

        self.stop_button.setEnabled(False)

        self.worker_thread = WorkerThread()
        self.worker_thread.signal.connect(self.on_thread_event)  # signal可以跨线程

    @QtCore.pyqtSlot()
    def on_stop_button_clicked(self):
        print('> request interrupt ...')
        self.worker_thread.requestInterruption()
        self.worker_thread.wait()

        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(False)

    @QtCore.pyqtSlot()
    def on_start_button_clicked(self):
        self.worker_thread.git_url = self.lineEdit.text()  # Get the git URL

        self.start_button.setEnabled(False)
        self.stop_button.setEnabled(True)

        self.textEdit.setText("starting thread ...\n")  # Updates the UI
        self.worker_thread.start()  # Finally starts the thread

    def on_thread_event(self, log):
        self.textEdit.setText(self.textEdit.toPlainText() + log)  # Show the output to the user

    def keyPressEvent(self, ev):
        if ev.key() == QtCore.Qt.Key_Escape:
            self.worker_thread.requestInterruption()
            self.worker_thread.wait()

            print('main exit.')
            QtWidgets.QApplication.quit()


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
