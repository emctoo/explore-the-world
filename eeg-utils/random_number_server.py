#!/usr/bin/env python
# coding: utf-8

import threading
import time
import random
import zmq
import logging
import uuid
import json
import sys
import numpy as np

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s')

class Step1Worker(threading.Thread):
    def __init__(self, data_url: str=None):
        super(Step1Worker, self).__init__()

        self.name = str(uuid.uuid4())
        self.is_running = threading.Event()

        self.context: zmq.Context = zmq.Context.instance()

        self.data_url: str = data_url
        self.data_socket: zmq.Socket = self.context.socket(zmq.PUB)

    def run(self):
        while not self.is_running.isSet:            
            self.data_socket.send(json.dumps(Step1Worker.sample()).encode('utf-8'))

    def join(self, timeout=None):
        self.is_running.set()
        super(Step1Worker, self).join(timeout)

    @staticmethod
    def sample(n_samples=random.randint(4, 10), n_channels=10):
        return list(np.random.uniform(size=(1, n_samples * n_channels)))

def main():
    name = '[main]'
    context: zmq.Context = zmq.Context.instance()

    data_url = 'tcp://192.168.0.43:2201'
    
    step1_thread: threading.Thread = None

    poller = zmq.Poller()
    poller.register(sys.stdin, zmq.POLLIN)

    sys.stdout.write('> ')
    sys.stdout.flush()
    while True:
        try:
            events = dict(poller.poll(10))
        except KeyboardInterrupt:
            print('\rbye.')
            break
            
        if events:
            print(events)

        if events.get(sys.stdin.fileno(), False):
            command_line = sys.stdin.readline().strip()
            logging.info(f'{name} process command: {command_line}')

            if command_line.startswith('start thread'):
                if step1_thread is None:
                    step1_thread: threading.Thread = Step1Worker(data_url)

                try:
                    step1_thread.start()
                except RuntimeError as e:
                    print('* {}'.format(e))

            elif command_line.startswith('kill thread'):
                step1_thread.join()
                step1_thread = None

                print(f'- thread stops')

            elif command_line.startswith('status'):
                if step1_thread is None:
                    print(f'no thread now, start first')
                else:
                    print(f'- {step1_thread.name}, is_alive: {step1_thread.is_running.is_set()}')

            else:
                logging.info('command unknown')
            sys.stdout.write('> ')
            sys.stdout.flush()

    logging.info(f'{name} exit now, thread {step1_thread.is_alive()}')

if __name__ == '__main__':
    main()
    