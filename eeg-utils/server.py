#!/usr/bin/env python
# coding: utf-8

import sys
import zmq


def main():
    context = zmq.Context()

    pub_socket = context.socket(zmq.REP)
    pub_socket.bind('tcp://*:2000')

    poller = zmq.Poller()
    poller.register(sys.stdin, zmq.POLLIN)
    poller.register(pub_socket, zmq.POLLIN)
    
    evts = dict(poller.poll(1000))

    print('> serving on :2000 ...')

    sys.stdout.write('> ')
    sys.stdout.flush()

    while True:
        try:
            evts = dict(poller.poll(1000))
        except KeyboardInterrupt:
            print('\r- bye')
            break

        stdin_ready = evts.get(sys.stdin.fileno(), False)
        if stdin_ready:
            line = sys.stdin.readline().strip()
            
            if line != '':
                print(f'- command: [{line}]')
            
            if line == 'bye':
                break

            sys.stdout.write('> ')
            sys.stdout.flush()
        

        pub_socket_ready = evts.get(pub_socket, False)
        if pub_socket_ready:
            req = pub_socket.recv_string()
            print(f'req: {req}')
            pub_socket.send_string(req.upper())

if __name__ == '__main__':
    main()