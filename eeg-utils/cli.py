#!/usr/bin/env python
# coding: utf-8

import sys
import zmq

def main():
    context = zmq.Context()
    
    req_socket = context.socket(zmq.REQ)
    req_socket.connect('tcp://localhost:2000')

    poller = zmq.Poller()
    poller.register(sys.stdin, zmq.POLLIN)
    poller.register(req_socket, zmq.POLLIN)

    sys.stdout.write('> ')
    sys.stdout.flush()
    
    while True:
        try:
            events = dict(poller.poll(1000))
        except KeyboardInterrupt:
            print('\r- bye.')
            break
        
        stdin_ready = events.get(sys.stdin.fileno(), False)
        if stdin_ready:        
            line = sys.stdin.readline().strip()
            print(f'- {line}')

            if line == 'bye':
                break

            req_socket.send_string(line)
            resp_string = req_socket.recv_unicode()
            print(f'- [from server] {resp_string}')

            sys.stdout.write('> ')
            sys.stdout.flush()

if __name__ == '__main__':
    main()