#!/usr/bin/env python
# coding: utf-8

import time
import random
import uuid
import sys
import zmq
import threading

class ImepdanceWorker:

    def __init__(self, worker_url: str, context: zmq.Context=None):
        self.name = str(uuid.uuid4())
        self.worker_url: str = worker_url
        
        self.context: zmq.Context = context or zmq.Context.instance()
        self.worker_socket: zmq.Socket = self.context.socket(zmq.PUB)
        
        self.is_running = threading.Event()

    def run(self):
        self.worker_socket.bind(self.worker_url)
        
        while not self.is_running.isSet:
            reply_string: str = worker_socket.recv_unicode()

            print('{} * {} * reply message {}'.format(self.name, time.strftime('%F.%T'), reply_string))
            if reply_string == 'exit':
                break

            time.sleep(random.random())
            worker_socket.send_string(reply_string.upper())


def eeg_sample_worker(eeg_sample_worker_url, context: zmq.Context):
    context = context or zmq.Context.instance()
    worker_socket: zmq.Socket = context.socket(zmq.PUB)
    worker_socket.connect(eeg_sample_worker_url)
    
    node = str(uuid.uuid4())

    while True:
        reply_string: str = worker_socket.recv_unicode()

        print('{} * {} * reply message {}'.format(node, time.strftime('%F.%T'), reply_string))
        if reply_string == 'exit':
            break

        time.sleep(random.random())
        worker_socket.send_string(reply_string.upper())

def execute_command(command_line: str) -> dict:
    default_response = {
        'success': True, 
        'output': 'ok', 
        'post_action': None,
    }

    if command_line != '':
        print(f'* command: [{command_line}]')
    
    if command_line.startswith('bye'):
        return {
            'success': True, 
            'output': 'bye.', 
            'post_action': 'quit',
        }
    
    # tokens = command_line.split()
    if command_line.startswith('start impedance server'):
        impedance_server_thread: threading.Thread = threading.Thread(target=impedance_sample_worker, args=('tcp://*:2001',))
        impedance_server_thread.start()

    if command_line.startswith('kill impedance server'):
        impedance_client_socket = zmq.Context.instance().socket(zmq.)

    return default_response


def main():
    context = zmq.Context()

    command_socket = context.socket(zmq.REP)
    command_socket.bind('tcp://*:2000')

    poller = zmq.Poller()
    poller.register(sys.stdin, zmq.POLLIN)
    poller.register(command_socket, zmq.POLLIN)
    
    events = dict(poller.poll(1000))

    print('* serving on :2000 ...')

    sys.stdout.write('> ')
    sys.stdout.flush()

    while True:
        try:
            events = dict(poller.poll(1000))
        except KeyboardInterrupt:
            print('\r* bye')
            break

        stdin_ready = events.get(sys.stdin.fileno(), False)
        if stdin_ready:
            response = execute_command(sys.stdin.readline().strip())
            print(f"* {response['output']}")

            sys.stdout.write('> ')
            sys.stdout.flush()

        pub_socket_ready = events.get(command_socket, False)
        if pub_socket_ready:
            response = command_socket.send_string(command_socket.recv_string())
            print(f"* {response['output']}")
            command_socket.send_string(response['output'])

if __name__ == '__main__':
    main()