defmodule SimpleWeb.MessageController do
    use SimpleWeb, :controller

    def insert(conn, params) do
        SimpleWeb.Endpoint.broadcast! "group:system", "events", params
        json conn, params
    end
end