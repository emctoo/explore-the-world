defmodule SimpleWeb.HelloController do
    use SimpleWeb, :controller
    require Logger

    def index(conn, _params), do: render conn, "index.html"

    def show(conn, %{"name" => name} = params) do
        Logger.info "params: #{inspect params, pretty: true}"
        render conn, "show.html", name: name
    end
end