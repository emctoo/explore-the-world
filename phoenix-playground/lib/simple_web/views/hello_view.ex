defmodule SimpleWeb.HelloView do
    use SimpleWeb, :view

    def what, do: "nothing"
end