defmodule SimpleWeb.GroupChannel do
  use Phoenix.Channel
  require Logger

  def join("group:system", _params, socket) do
    Logger.info "joins system group"

    :timer.send_interval(1000, :system_time)
    {:ok, socket}
  end

  def handle_info(:system_time, socket) do
    push(socket, "system_time", %{"now" => now()})
    {:noreply, socket}
  end

  defp now do
    Calendar.DateTime.now!("Asia/Shanghai") |> Calendar.Strftime.strftime!("%F %T.%z")
  end

  def handle_in("system_event", message, socket) do
    Logger.info "group:system/system_event, #{inspect message}"

    broadcast! socket, "system_event", message
    {:noreply, socket}
  end
end
