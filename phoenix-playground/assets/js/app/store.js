import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
import Fingerprint2 from 'fingerprintjs2';

import socket from '../socket';

let store = null;

function initStateChannel(state, onJoinSuccess, onJoinError) {
    let channel = socket.channel('group:system', {})
    channel.join()
        .receive('ok', resp => {
            console.log(`joins group:system successfully`);
            console.dir(resp);
        })
        .receive('error', err => { console.log(`joins ${state.channelName} errors, ${err}`); })

    channel.on('system_time', ({ now }) => {
        store.dispatch('update_system_time', { now });
    });

    channel.on('system_event', payload => {
        console.log(`system_event`);
        console.dir(payload);
    })

    console.log(`channel join_ref: ${channel.joinRef()}`);
    return channel;
}


const state = {
    channel: null,
    fingerprint: null,
    counter: 42,
    system_time: ''
}

function createState() {
    console.log(`init state`);
    // if (state.ws === null) state.ws = createWs();

    if (state.fingerprint === null) {
        const options = {};
        Fingerprint2.get(options, components => {
            console.dir(components);
            const hash = Fingerprint2.x64hash128(components.map(component => component.value).join(''), 31);
            store.dispatch('update_fingerprint', hash);
        })
    }

    if (state.channel === null) state.channel = initStateChannel(state);
    return state
}

const getters = {
    fingerprint: (state) => state.fingerprint,
    counter: (state) => state.counter,
    system_time: (state) => state.system_time,
}

const mutations = {
    update_fingerprint(state, value) { state.fingerprint = value; },
    update_counter(state, value) { state.counter = value; },
    update_system_time(state, { now }) { state.system_time = now; }
}

const actions = {
    update_fingerprint: async ({ state, commit }, value) => {
        commit('update_fingerprint', value);
    },
    update_counter: async ({ state, commit }, value) => {
        commit('update_counter', value);
        state.channel
            .push('system_event', { source: state.fingerprint, target: 'broadcast', event: 'updating_counter', value })
            .receive('ok', resp => { console.log(`successfully sending state`) })
            .receive('error', err => { console.log(`error happens, ${err}`) })
    },
    update_system_time: ({ state, commit }, { now }) => {
        commit('update_system_time', { now })
    }
}

const PhoenixPlugin = store => {
    // 当 store 初始化后调用
    store.subscribe((mutation, state) => {
        // 每次 mutation 之后调用
        // mutation 的格式为 { type, payload }
    })
}

store = new Vuex.Store({ state: createState(), getters, mutations, actions, plugins: [PhoenixPlugin] })
export default store;