import Vue from 'vue';

import Vuex from 'vuex';
Vue.use(Vuex);

import Fingerprint2 from 'fingerprintjs2';

import socket from '../socket';
let store = null;

function initStateChannel(state, onJoinSuccess, onJoinError) {
    let channel = socket.channel(state.channelName, {})
    channel.join()
        .receive('ok', resp => {
            console.log(`joins group:system successfully`);
            console.dir(resp);
        })
        .receive('error', err => { console.log(`joins ${state.channelName} errors, ${err}`); })

    channel.on('system_time', ({ now }) => {
        store.dispatch('update_system_time', { now });
    });

    channel.on('system_event', payload => {
        console.log(`system_event`);
        console.dir(payload);
    })

    console.log(`channel join_ref: ${channel.joinRef()}`);
    return channel;
}

const actions = {
    handleChannelJoin: ({ state, commit, getters }, resp) => {
        console.log(`joins ${state.channelName}`);
    },
    update_fingerprint: async ({ state, commit }, value) => {
        commit('update_fingerprint', value);
    },
    update_system_time: ({ state, commit }, { now }) => { commit('update_system_time', now) },
    update_counter: async ({ state, commit }, { value }) => {
        console.log(`system time: ${state.system_time}`)
        commit('update_counter', value);

        payload = {
            source: state.fingerprint,
            target: 'broadcast',
            system_time: state.system_time,
            event: 'updating_counter',
            value,
        }
        state.channel
            .push('system_event', payload)
            .receive('ok', resp => { console.log(`successfully sending state`) })
            .receive('error', err => { console.log(`error happens, ${err}`) })
    },
    append_event: ({ commit }, event) => { commit('append_event', event) }
}

const getters = {
    isChannelReady: (state) => state.channel !== null,
    fingerprint: (state) => state.fingerprint,
    system_time: (state) => state.system_time,
    events: (state) => state.events
}

const state = {
    socket,
    channelName: 'group:system',
    fingerprint: null,
    channel: null,
    system_time: '',
    counter: 0,
    events: [],
}


const mutations = {
    update_fingerprint(state, value) { state.fingerprint = value; },
    update_system_time: (state, now) => state.system_time = now,
    update_counter: (state, value) => state.counter = value,
    append_event: (state, event) => state.events.push(event)
}

function initState() {
    console.log('initialize state');

    if (state.fingerprint === null) {
        const options = {};
        Fingerprint2.get(options, components => {
            console.dir(components);
            const hash = Fingerprint2.x64hash128(components.map(component => component.value).join(''), 31);
            store.dispatch('update_fingerprint', hash);
        })
    }

    if (state.channel === null) state.channel = initStateChannel(state);
    return state;
}

store = new Vuex.Store({ state: initState(), getters, actions, mutations })
// store.state.channel.on('system_time', ({ now }) => {
//     console.log(`system_time: ${now}`);
//     store.commit('updateDateTime', now);
// })

export default store;