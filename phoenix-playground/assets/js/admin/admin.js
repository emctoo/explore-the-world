import Vue from 'vue';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
import Admin from './Admin.vue';

import store from './store'

new Vue({ el: '#app', render: h => h(Admin), store })