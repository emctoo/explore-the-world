import Fingerprint2 from 'fingerprintjs2';

async function fingerprint() {
    new Promise((resolve, reject) => {
        const options = {};
        Fingerprint2.get(options, components => {
            console.dir(components);
            const hash = Fingerprint2.x64hash128(components.map(component => component.value).join(''), 31);
            console.log(`hash value: ${hash}, len: ${hash.length}`);
            resolve(hash);
        })
    });
}

export default fingerprint;