#!/usr/bin/env python
# coding: utf8

import asyncio, datetime
import sanic
from sanic import request, response
from sanic.log import logger

import websockets.exceptions

app = sanic.Sanic()


@app.listener('before_server_start')
async def before_server_start(app, loop):
    app.USERS = set()


async def push_system_time(ws):
    now = datetime.datetime.now().strftime('%F.%T')
    logger.info('send %s', now)
    ws.send(now)


async def consumer_handler(ws, loop, app):
    try:
        async for message in ws:
            logger.info('receive message from %s, %s', ws, message)

    except websockets.exceptions.ConnectionClosed as ex:
        logger.info('connection closed')

    finally:
        logger.info('client close')


async def producer_handler(ws, loop, app):
    try:
        while True:
            now = datetime.datetime.now().strftime('%F.%T')
            # logger.info('send %s', now)
            await ws.send(now)
            await asyncio.sleep(1)
    except websockets.exceptions.ConnectionClosed as ex:
        logger.info('connection closed')
    finally:
        logger.info('client closed')


async def handler(ws, loop, app):
    app.USERS.add(ws)

    consumer_task = loop.create_task(consumer_handler(ws, loop, app))
    producer_task = loop.create_task(producer_handler(ws, loop, app))
    done, pending = await asyncio.wait([consumer_task, producer_task],
                                       return_when=asyncio.FIRST_COMPLETED)

    # 等着这两个 task 结束，每个 ws 连接都会创建
    for task in pending:
        task.cancel()

    app.USERS.remove(ws)


async def feed(req, ws):
    await handler(ws, req.app.loop, req.app)

async def hello(req):
    return response.text('hello')

async def ping(req):
    args = {**(req.json or {}), **(req.query_args or {}), **dict(req.headers)}
    return response.json({'success': True, 'payload': args})

app.static('/index.html', './public/index.html')
app.static('/', './public/index.html')
app.static('/favicon.ico', './public/favicon.ico')

app.add_websocket_route(feed, '/feed')

app.add_route(hello, '/hello', ['GET', 'POST'])
app.add_route(ping, '/ping', ['GET', 'POST'])

app.run(host='0.0.0.0', debug=True)
