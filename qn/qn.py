#!/usr/bin/env python
# coding: utf8

import sys, json
from qiniu import Auth, put_file

import requests
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def gen_qn_token():
    # 需要填写你的 Access Key 和 Secret Key
    access_key = 'AS2tygAxphfoA5LdwxltAJiKWqqc7C-dBiPwneFe'
    secret_key = 'V1AG5Z89X43FVM9EVMzI_l5cqVFtUbsaYi3SWt0p'

    # 构建鉴权对象
    q = Auth(access_key, secret_key)

    # 要上传的空间
    bucket_name = 'closer-images'

    # 生成上传 Token，可以指定过期时间等
    userId = 1
    policy = {
        'callbackUrl': 'http://cmc.masonsoft.com:8080/qn/upload-callback',  # TODO 生产环境配置
        'callbackBody': f'filename=$(fname)&filesize=$(fsize)&hash=$(etag)&user-id={userId}',
    }
    return q.upload_token(bucket_name, None, 3600, policy)


class Closer:

    def __init__(self):
        self.host = 'http://cmc.masonsoft.com:8080'
        self.user_id, self.token = self.__login()

    def __login(self, telephone='8615201923055', password='222222'):
        payload = {
            'type': 'telephone',
            'telephone': telephone,
            'password': password,
        }

        logging.info('%s/%s login now ...', telephone, password)
        resp = requests.post(f'{self.host}/login', json=payload)
        if resp.status_code != 200 or resp.json()['success'] != True:
            logging.info('login error, resp: %s', resp)
            return None
        values = resp.json()['payload']
        return values['user_id'], values['token']

    def api(self, uri, payload={}):
        url = f'{self.host}{uri}'
        headers = {'Authorization': f'Bearer {self.token}'}
        resp = requests.post(url, json=payload, headers=headers)

        if resp.status_code != 200:
            logging.info('request error, status code: %s', resp.status_code)
            return

        if resp.json()['success'] == False:
            logging.info('request error, error: %s', resp.json()['payload'])
            return

        return resp.json()['payload']

    def user_profile(self):
        values = self.api('/api/users/profiles')
        logging.info('request photos response: ')
        print(json.dumps(values, indent=2))


    def list_photos(self):
        url = f'{self.host}/api/resources/user-photos'
        resp = requests.post(url, headers={'Authorization': f'Bearer {self.token}'})
        if resp.status_code != 200:
            logging.info('request error, status code: %s', resp.status_code)
            return
        if resp.json()['success'] == False:
            logging.info('request error, error: %s', resp.json()['payload'])
            return

        values = resp.json()['payload']
        logging.info('request photos response: ')
        print(json.dumps(values, indent=2))

    def delete_photo(self, resource_id):
        url = f'{self.host}/api/resources/user-photos/delete'
        payload = {'resource_id': resource_id}
        resp = requests.post(url, json=payload, headers={'Authorization': f'Bearer {self.token}'})
        if resp.status_code != 200:
            logging.info('request error, status code: %s', resp.status_code)
            return
        if resp.json()['success'] == False:
            logging.info('request error, error: %s', resp.json()['payload'])
            return

        values = resp.json()['payload']
        logging.info('deleting photo %s', resource_id)
        print(json.dumps(values, indent=2))

    def fetch_qiniu_upload_token(self, *, is_avatar=False, replace=False, resource_id=''):
        logging.info('request for qn token ...')
        url = f'{self.host}/qn/token'
        payload = {
            'is_avatar': 1 if is_avatar else 0,
            'replace': 1 if replace else 0,
            'resource_id': resource_id,
        }

        resp = requests.post(url, json=payload, headers={'Authorization': f'Bearer {self.token}'})
        if resp.status_code != 200 or resp.json()['success'] != True:
            logging.info('error fetching qn token, %s', resp)
            return None

        values = resp.json()['payload']
        return values['token']

    def upload_photo(self, localfile, *, is_avatar=False, replace=True, resource_id=''):
        qn_token = self.fetch_qiniu_upload_token(is_avatar=is_avatar, replace=replace, resource_id=resource_id)
        ret, info = put_file(qn_token, None, localfile)  # no key, set by server
        logging.info('ret: %s', ret)
        logging.info('upload information: %s', info)
        # print(json.dumps(info))


def main(localfile):
    pass


if __name__ == '__main__':
    # 要上传文件的本地路径
    localfile = sys.argv[1] if len(sys.argv) > 1 else '/home/maple/Downloads/first.jpg'
    main(localfile)
