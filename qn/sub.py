#!/usr/bin/env python
# coding: utf8

import asyncio
import aiormq
import aiormq.types

import json
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


async def on_message(message: aiormq.types.DeliveredMessage):
    logging.info("message body")
    values = json.loads(message.body.decode())
    print(json.dumps(values, indent=2))
    await message.channel.basic_ack(message.delivery.delivery_tag)


async def work():
    # Perform connection
    connection = await aiormq.connect("amqp://admin:17380538161@127.0.0.1")

    # Creating a channel
    channel = await connection.channel()
    await channel.basic_qos(prefetch_count=1)
    await channel.exchange_declare(exchange='logs', exchange_type='fanout')

    # declaring queue
    declare_ok = await channel.queue_declare(exclusive=True)

    # binding the queue to the exchange
    await channel.queue_bind(declare_ok.queue, 'logs')

    # start listening the queue with name 'task_queue'
    await channel.basic_consume(declare_ok.queue, on_message)


def main():
    loop = asyncio.get_event_loop()
    loop.create_task(work())

    # we enter a never-ending loop that waits for data and runs callbacks whenever necessary.
    logging.info('waiting for logs ...')
    loop.run_forever()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\rbye.')
