#!/usr/bin/env python
# coding: utf-8

import sanic

app = sanic.Sanic(__name__)

@app.post('/file-updated')
async def fileUpdated(req):
    print(req.json)
    return sanic.response.json({'success': True})

if __name__ == "__main__":
    app.run(host='localhost', port=1986, debug=True)