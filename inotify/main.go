package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/fsnotify/fsnotify"
)

func req(url string, message map[string]interface{}) {
	buf, err := json.Marshal(message)
	if err != nil {
		log.Fatalln(err)
	}

	contentType := "application/json"
	resp, err := http.Post(url, contentType, bytes.NewBuffer(buf))
	if err != nil {
		log.Fatalf("http request error: %s\n", err)
	}

	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	log.Println("result: ", result)
}

func watchAndFireRequest(url string, watcher *fsnotify.Watcher, done chan bool) {
	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}
			log.Println("event:", event)
			if event.Op&fsnotify.Write == fsnotify.Write {
				message := map[string]interface{}{
					"event": event.Op.String(),
					"file":  event.Name,
				}
				req(url, message)
				log.Println("modified file:", event.Name)
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			log.Println("error:", err)
		}
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("%s <url> <dir>\n", os.Args[0])
		return
	}

	url := os.Args[1]
	directory := os.Args[2]

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go watchAndFireRequest(url, watcher, done)

	err = watcher.Add(directory)
	if err != nil {
		log.Fatal(err)
	}
	<-done
}
