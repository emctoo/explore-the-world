module notify

require (
	github.com/fsnotify/fsnotify v1.4.7
	golang.org/x/sys v0.0.0-20190123074212-c6b37f3e9285 // indirect
	gopkg.in/urfave/cli.v1 v1.20.0 // indirect
	gopkg.in/urfave/cli.v2 v2.0.0-20180128182452-d3ae77c26ac8 // indirect
)
