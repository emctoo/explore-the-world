#!/usr/bin/env python
# coding: utf-8

import os
import asyncio
import logging
import openpyxl as xl
import aionotify

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def load_xlsx():
    fname = '/tmp/devel/fr.xlsx'

    wb = xl.load_workbook(fname)
    print(wb.sheetnames)


async def monitor_cwd(name=None):
    watcher = aionotify.Watcher()

    # 如果alias是None, 会用当前目录
    watcher.watch(alias=name, path=os.getcwd(), flags=aionotify.Flags.MODIFY)

    cmcs_options = {
        'alias': 'cmcs',
        'path': '/home/maple/cmcs-system',
        'flags': aionotify.Flags.MODIFY,
    }
    watcher.watch(**cmcs_options)
    await watcher.setup(asyncio.get_event_loop())

    while True:
        ev = await watcher.get_event()
        logging.info('event: %s', ev)
        if alias == 'cmcs':
            print('wow ...')
    watcher.close()


def main():
    asyncio.run(monitor_cwd())


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('\rbye.')