package main

import (
	// "database/sql"
	"errors"
	"log"
	"math/rand"
	"net/http"
	// "os"
	"time"

	_ "github.com/mattn/go-sqlite3"

	"github.com/dgrijalva/jwt-go"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func login(c echo.Context) (err error) {
	type user struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var u user
	if err := c.Bind(&u); err != nil {
		log.Fatalln("bad request parameters")
		return nil
	}

	if u.Username == "test" && u.Password == "test" {
		// Create token
		token := jwt.New(jwt.SigningMethodHS256)

		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["name"] = "Jon Snow"
		claims["admin"] = true
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, map[string]interface{}{"token": t, "id": rand.Int31n(10)})
	}

	return echo.ErrUnauthorized
}

// UserManager 用户管理接口
type UserManager interface {
	Exists(name string) bool
	Login(username string, id int32) error
	Logout(username string) error
}

// UserDict 用户及id列表
type UserDict map[string]int32

// Exists user是否已经登录
func (ud UserDict) Exists(username string) bool {
	_, exists := ud[username]

	log.Printf("check user [%s] existence: %t\n", username, exists)
	return exists
}

// Login user登录
func (ud UserDict) Login(username string, id int32) error {
	if ud.Exists(username) {
		log.Println("user exists, can not login")
		return errors.New("user exists")
	}

	log.Printf("add username [%s], with id [%d]\n", username, id)
	ud[username] = id
	return nil
}

// Logout user登出
func (ud UserDict) Logout(username string) error {
	if ud.Exists(username) {
		log.Println("user exists, can not login")
		return errors.New("user exists")
	}

	log.Printf("delete username [%s]\n", username)
	delete(ud, username)
	return nil
}

func startUserManager(ud UserDict, op string, username string, id int32) error {
	return nil
}

// Service 服务
type Service struct {
	users map[string]int32
}

// LoggedIn 是否已经登录
func (s *Service) LoggedIn(username string) bool {
	_, exists := s.users[username]
	return exists
}

func (s *Service) login(ctx echo.Context) error {
	type user struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	var u user
	if err := ctx.Bind(&u); err != nil {
		log.Fatalln("bad request parameters")
		return nil
	}

	if !s.LoggedIn(u.Username) {
		log.Printf("user already LoggedIn.")
		return ctx.String(200, "already logged in")
	}

	log.Printf("(%s/%s) login.\n", u.Username, u.Password)
	jwtTokenGenerator := jwt.New(jwt.SigningMethodHS256)

	claims := jwtTokenGenerator.Claims.(jwt.MapClaims)
	claims["username"] = u.Username
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	token, err := jwtTokenGenerator.SignedString([]byte("secret"))
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, map[string]interface{}{"success": true, "token": token})
}

func main() {
	// os.Remove("./foo.db")

	// db, err := sql.Open("sqlite3", "./foo.db")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// defer db.Close()

	s := new(Service)

	log.Println("hello, world!")
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.POST("/api/users/login", s.login)
	e.Logger.Fatal(e.Start(":2000"))
}

func init() {
	log.Println("init created")
}
