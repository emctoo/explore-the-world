﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using System.Text;
using System.IO;

namespace HelloWorld {
    class Program {
        static TextReader stdinReader = Console.In;
        static StringBuilder inputStringBuilder = new StringBuilder();
        static HubConnection conn = null;
        static string name = new Random().NextDouble().ToString().Substring(2, 3); // Guid.NewGuid().ToString();
        static string mode = "broadcast";

        private static async Task readline() {
            Console.Write($"{name}> ");

            while (true) {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                char ch = keyInfo.KeyChar;

                Console.WriteLine($"ch: {ch} [{ch.ToString()}], current: [{inputStringBuilder.ToString()}]");

                if (ch == -1) break;
                if (ch == '\b') {
                    inputStringBuilder.Remove(inputStringBuilder.Length - 1, 1);
                    continue;
                }

                if (ch == '\r' || ch == '\n') {
                    if (ch == '\r' && Console.Peek() == '\n') Console.Read();

                    var items = inputStringBuilder.ToString().Trim().Split(" ", 2);
                    if (items.Length < 2) continue;


                    inputStringBuilder.Clear();
                    if (items[0] == "set-name") {
                        name = items[1];
                    } else {
                        await conn.InvokeAsync("SendMessage", items[0], items[1]);
                    }

                    Console.Write($"{name}> ");
                }

                inputStringBuilder.Append((char)ch);
            }
        }

        private static async Task t() {
            while (true) {
                var line = ReadLine.Read($"({name})> ");
                Console.WriteLine($"- [{line}]");

                var items = line.Trim().Split(" ", 2);

                if (items.Length < 1) {
                    Console.WriteLine("bad command");
                    continue;
                }

                var command = items[0];

                if (items.Length < 2) {
                    Console.WriteLine("bad command");
                    continue;
                }

                if (command == "set-name") {
                    name = items[1];
                    Console.WriteLine($"set name to {name}");
                } else if (command == "set-mode") {
                    mode = items[1];
                    Console.WriteLine($"set mode to {mode}");
                } else {
                    await conn.InvokeAsync("SendMessage", items[0], items[1]);
                }
            }
        }


        // 核心问题: 输入的过程中有输出
        static async Task Main(string[] args) {
            Console.WriteLine($"name: {name}, default mode: {mode}");

            conn = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/ChatHub")
                .Build();

            conn.Closed += async (error) => {
                // 出错时等待若干时间，重连
                Console.WriteLine($"connection closed, error: {error}");

                await Task.Delay(new Random().Next(0, 5) * 1000);
                await conn.StartAsync();
            };

            conn.On<string, string>("ReceiveMessage", (user, message) => {
                Console.WriteLine($"\r- message from [{user}], message: [{message}]");
                if (inputStringBuilder.Length > 0) Console.WriteLine($"> {inputStringBuilder.ToString()}");
            });

            await conn.StartAsync();

            await readline();
            // await t();
        }
    }
}
