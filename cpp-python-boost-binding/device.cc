#include <iostream>
#include <random>
#include <cstdlib> // #include <stdlib.h>

namespace device {

class buffer {
public:
  buffer() {
    channelCount = 10;
    sampleCount = random_unsigned_int();
    raw_data_buffer = (double *)malloc(sizeof(double) * channelCount * MAX_SAMPLE_COUNT);
    std::cout << "buffer created\n";
  }

  ~buffer() {
    free(raw_data_buffer);
    std::cout << "buffer destroyed\n";
  }

  unsigned int getChannelCount() { return channelCount; }

  double *data() {
    sampleCount = random_unsigned_int();
    unsigned int size = sampleCount * channelCount;
    for (unsigned nth = 0; nth < size; ++nth) raw_data_buffer[nth] = random_double();
    return raw_data_buffer;
  }

  size_t size() const { return sampleCount * channelCount; }

private:
  // rand() -> [0, RAND_MAX]
  // return [50, 80]
  unsigned int random_unsigned_int() { return 50 + rand() % 30; }

  double random_double() { return double(rand()) / double(RAND_MAX); }

private:
  const int MAX_SAMPLE_COUNT = 80;
  unsigned int channelCount;
  unsigned int sampleCount;
  double *raw_data_buffer;
};

class stream {
public:
  stream() {
    this->name = "eeg";
    std::cout << "stream (default eeg) created\n";
  }
  stream(std::string name) {
    this->name = name;
    std::cout << "stream " << this->name << " created\n";
  }

  ~stream() { std::cout << "stream destroyed\n"; }

  std::string getName() { return this->name; }

  buffer &getData() { return name == "eeg" ? this->eegBuffer : this->impedanceBuffer; }

private:
  std::string name;
  buffer impedanceBuffer;
  buffer eegBuffer;
};

class amplifier {
public:
  amplifier() { std::cout << "amplifier created\n"; }
  ~amplifier() { std::cout << "amplifier destroyed\n"; }

  stream *OpenImpedanceStream() { return new stream; }
};

class factory {
public:
  factory(std::string lib) {
    std::cout << "factory created, lib " << lib << "\n";
    std::cout << "factory shared library loaded\n";
  }

  ~factory() { std::cout << "factory destroyed\n"; }

  amplifier *getAmplifier() { return new amplifier; }
};

} // namespace device
