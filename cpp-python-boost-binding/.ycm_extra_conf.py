import os

flags = [
    '-Wall',
    '-Wextra',
    '-Werror',
    '-fexceptions',
    '-std=c++1z',
    '-xc++',
    '-isystem/usr/include'
]
