#!/usr/bin/env python
# coding: utf-8

import zmq
import msgpack

def main():
    context = zmq.Context()
    
    subscribe_socket = context.socket(zmq.SUB)
    subscribe_socket.connect('tcp://localhost:2898')
    subscribe_socket.setsockopt(zmq.SUBSCRIBE, b"")

    poller = zmq.Poller()
    poller.register(subscribe_socket, zmq.POLLIN)

    print('> start polling ...')
    while True:
        try:
            events = dict(poller.poll(10))
        except KeyboardInterrupt:
            print('\rbye.')
            break

        subscribe_socket_ready = events.get(subscribe_socket, False)
        if subscribe_socket_ready:
            payload = msgpack.unpackb(subscribe_socket.recv())
            print('{} - n_channels: {}, n_samples: {}, count: {}'.format(payload[0], payload[1], payload[2], len(payload[3])))
            

if __name__ == '__main__':
    main()
