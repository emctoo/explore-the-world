#!/usr/bin/env python
# coding: utf-8 

import zmq
import devices

def main():
    print(f'version: {devices.version()}')

    with devices.Amplifier('fuck-the-world') as amp:
        print('manage device with context manager')
        with amp.impedanceStream() as imp:
            print('inner things')

if __name__ == '__main__':
    print('-' * 80 + ' test')
    main()
