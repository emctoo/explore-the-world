#include <boost/python.hpp>
#include <iostream>

#include "device.h"

using namespace boost::python;

const char *version() { return "1.0"; }

class ImpedanceStream {
public:
	ImpedanceStream(device::amplifier *amp): amp(amp) {}
	~ImpedanceStream() = default; 

	void __enter__() {
	  if (impedanceStream != nullptr) {
		  std::cout << "不能重复获取impedance stream实例\n";
		  return; // TODO: throw exception?
	  }

	  impedanceStream = amp->OpenImpedanceStream();
	}

	bool __exit__(object ex_type, object ex_value, object tb) {
		delete impedanceStream; 
		impedanceStream = nullptr;

		return true;
	}

private:
	device::amplifier *amp = nullptr;
	device::stream *impedanceStream = nullptr;
};

class Amplifier {
public:
	Amplifier(): Amplifier("hello") {}
	Amplifier(std::string lib) {
		_factory.load_library(lib);
	}

	~Amplifier() = default;

  	void __enter__() {
		_amplifier = _factory.getAmplifier();	
	}

	bool __exit__(object ex_type, object ex_value, object tb) {
		delete _amplifier;

		return true;
	}

	ImpedanceStream impedanceStream() { return ImpedanceStream(_amplifier); }

private:
	device::factory _factory;
	device::amplifier *_amplifier;
};

BOOST_PYTHON_MODULE(devices) {

  def("version", version);

  class_<Amplifier>("Amplifier", init<std::string>())
	.def(init<>())
	.def("__enter__", &Amplifier::__enter__)
	.def("__exit__", &Amplifier::__exit__)
 	.def("impedanceStream", &Amplifier::impedanceStream);

  // class_<ImpedanceStream>("ImpedanceStream", init<>())
}
