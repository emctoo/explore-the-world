#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <signal.h>

#include <msgpack.hpp>
#include "cppzmq/zmq.hpp"

#include "device.h"

double time_ns() { 
	return double(
		std::chrono::high_resolution_clock::now().time_since_epoch().count()) / 1000000000.0; 
}

struct SampleData {
	SampleData(device::buffer *_buffer) {
		timestamp = time_ns();
		n_channels = _buffer->getChannelCount();
		n_samples = _buffer->getSampleCount();
		
		auto _raw_buffer = _buffer->data();
		values.insert(values.end(), _raw_buffer, _raw_buffer + _buffer->size());
	}

	double timestamp;
	unsigned int n_channels;
	unsigned int n_samples;
	std::vector<double> values;

	MSGPACK_DEFINE(timestamp, n_channels, n_samples, values);
};

static _Atomic bool is_interrupted = false;
void on_user_interrupt(int signo) { is_interrupted = true; }

int main() {
	zmq::context_t context;
	zmq::socket_t publish_socket(context, ZMQ_PUB);

	publish_socket.bind("tcp://*:2898");

	device::factory fact;	
	fact.load_library("hello, world!");

	auto amp = fact.getAmplifier();
	auto impedance_stream = amp->OpenImpedanceStream();
	
	signal(SIGINT, on_user_interrupt);

	while (is_interrupted == false) {
		SampleData sample_data(impedance_stream->getData());

		msgpack::sbuffer sbuf;
		msgpack::pack(sbuf, sample_data);
		
		publish_socket.send(sbuf.data(), sbuf.size());
		
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}

	delete impedance_stream;
	delete amp;
	return 0;
}
