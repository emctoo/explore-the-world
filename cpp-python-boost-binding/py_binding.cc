#include <pybind11/pybind11.h>
#include <iostream>

#include "device.h"
using namespace device;

namespace py = pybind11;

const char *version() { return "1.0"; }

// singlton
class ImpedanceStream {
public:
 	static ImpedanceStream* builder(device::amplifier *amp) {
		if (ImpedanceStream::instance == nullptr) ImpedanceStream::instance = new ImpedanceStream(amp);
		return ImpedanceStream::instance;
	}

	ImpedanceStream() {
		_factory.load_library("hello");
	}

	void __enter__() {
	  std::cout << "allocating new impedance stream\n";

	  _amplifier = _factory.getAmplifier();
	  _impedanceStream = _amplifier->OpenImpedanceStream();
	}

	bool __exit__(py::object ex_type, py::object ex_value, py::object tb) {
		delete _impedanceStream; 
		delete _amplifier;

		std::cout << "destory device impedance stream\n";
		return true;
	}

	double value() { return 42.0; }

private:
	ImpedanceStream(device::amplifier *amp) { this->_amplifier = amp; }

private:
	static ImpedanceStream *instance;

	device::factory _factory;
	device::amplifier *_amplifier = nullptr;
	device::stream *_impedanceStream = nullptr;
};

class Amplifier {
public:
	Amplifier(): Amplifier("hello") {}
	Amplifier(std::string lib) {
		_factory.load_library(lib);
	}

	~Amplifier() = default;

  	void __enter__() {
		_amplifier = _factory.getAmplifier();	
	}

	bool __exit__(py::object ex_type, py::object ex_value, py::object tb) {
		delete _amplifier;
		return true;
	}

	ImpedanceStream* impedanceStream() { 
		std::cout << "visiting impedance stream\n"; 
		auto instance = ImpedanceStream::builder(_amplifier);
		if (instance == nullptr) {
			std::cout << "error happens ...\n";
			return nullptr;
		}

		return instance;
	}

private:
	device::factory _factory;
	device::amplifier *_amplifier;
};

PYBIND11_MODULE(devices, m) {
  m.def("version", &version, "sdk version");

  py::class_<Amplifier>(m, "Amplifier")
	.def(py::init<std::string>())
	.def(py::init<>())
	.def("__enter__", &Amplifier::__enter__)
	.def("__exit__", &Amplifier::__exit__)
 	.def("impedanceStream", &Amplifier::impedanceStream); 

  py::class_<ImpedanceStream, std::unique_ptr<ImpedanceStream, py::nodelete>>(m, "ImpedanceStream")
	.def("value", &ImpedanceStream::value);
}
