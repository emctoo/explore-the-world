#ifndef device_h
#define device_h

#include <cstdlib> // #include <stdlib.h>
#include <iostream>
#include <random>

namespace device {

static const unsigned int MIN_SAMPLE_COUNT = 10;
static const unsigned int MAX_SAMPLE_COUNT = 20;

class buffer {
public:
  buffer(std::string stream_type) {
    this->stream_type = stream_type;

    channelCount = 10;
    sampleCount = random_unsigned_int();
    raw_data_buffer = (double *)malloc(sizeof(double) * channelCount * MAX_SAMPLE_COUNT);
    std::cout << "buffer created\n";
  }

  ~buffer() {
    free(raw_data_buffer);
    std::cout << "buffer destroyed\n";
  }

  unsigned int getChannelCount() { return channelCount; }
  unsigned int getSampleCount() { return sampleCount; }

  double *data() {
    sampleCount = random_unsigned_int();
    unsigned int size = sampleCount * channelCount;
    for (unsigned nth = 0; nth < size; ++nth) raw_data_buffer[nth] = random_double();
    return raw_data_buffer;
  }

  size_t size() const { return sampleCount * channelCount; }

private:
  std::string stream_type;

  // rand() -> [0, RAND_MAX]
  // return [50, 80]
  unsigned int random_unsigned_int() { return MIN_SAMPLE_COUNT + rand() % (MAX_SAMPLE_COUNT - MIN_SAMPLE_COUNT); }

  double random_double() { return double(rand()) / double(RAND_MAX); }

private:
  unsigned int channelCount;
  unsigned int sampleCount;
  double *raw_data_buffer;
};

class stream {
public:
  stream(): stream("impedance") {}

  stream(std::string name) {
    this->name = name;
    std::cout << "stream [" << this->name << "] created\n";

    this->_buffer = new buffer(this->name);
  }

  ~stream() { 
    delete this->_buffer;
    std::cout << "stream [" << this->name << "] destroyed\n"; 
  }

  std::string getName() { return this->name; }

  buffer* &getData() { return this->_buffer; }

private:
  std::string name;
  buffer *_buffer;
};

class amplifier {
public:
  amplifier() { std::cout << "amplifier created\n"; }
  ~amplifier() { std::cout << "amplifier destroyed\n"; }

  stream *OpenImpedanceStream() { return new stream("impedance"); }
};

class factory {
public:
  factory() = default;

  factory(std::string lib) {
    std::cout << "factory created, lib [" << lib << "]\n";
    std::cout << "factory shared library loaded\n";
  }

  ~factory() { std::cout << "factory destroyed\n"; }

  void load_library(std::string lib) {
    std::cout << "load library [" << lib << "]\n";
  }
  amplifier *getAmplifier() { return new amplifier; }
};

} // namespace device

#endif // device_h
