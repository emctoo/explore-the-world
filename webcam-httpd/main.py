#!/usr/bin/env python
# coding=utf-8

import asyncio, random, functools
import sanic
from sanic import response
import cv2


class Camera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)

    def __del__(self):
        self.video.release()

    def get_frame(self):
        success, image = self.video.read()
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()


# app = Flask(__name__)
app = sanic.Sanic('camera')


@app.get('/')
async def index(request):
    return await response.file('index.html')


@app.get('/favicon.ico')
async def index(request):
    return await response.file('favicon.ico')


FPS = 29.97


async def gen(camera, response):
    """Video streaming generator function."""
    loop = asyncio.get_event_loop()
    while True:
        frame = await loop.run_in_executor(None, camera.get_frame)
        await response.write(b'--frame\r\n'
                             b'Content-Type: image/jpeg\r\n\r\n' + frame +
                             b'\r\n')
        await asyncio.sleep(1.0 / FPS)


@app.route('/video-feed')
def video_feed(request):
    """Video streaming route. Put this in the src attribute of an img tag."""
    return response.stream(
        functools.partial(gen, Camera()),
        content_type='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)