# TcpServer

**TODO: Add description**
https://medium.com/blackode/quick-easy-tcp-genserver-with-elixir-and-erlang-10189b25e221
http://erlang.org/doc/man/gen_tcp.html
https://robots.thoughtbot.com/playing-with-sockets-and-processes-in-elixir

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `gen_tcp` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:gen_tcp, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/gen_tcp](https://hexdocs.pm/gen_tcp).

