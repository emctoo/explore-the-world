defmodule TcpServer do
  use GenServer
  require Logger

  def start_link do
    ip = Application.get_env(:gen_tcp, :ip, {127, 0, 0, 0}) # default to localhost
    port = Application.get_env(:gen_tcp, :port, 6666) # default to 6666
    GenServer.start_link(__MODULE__, {ip, port}, name: __MODULE__)
  end

  def init({ip, port}) do
    # 接收binary
    # 发送到mailbox; 否则passive mode, 需要调用:gen_tcp:recv/2,3
    options = [:binary, {:packet, 0}, {:active, true}, {:ip, ip}]
    {:ok, listen_socket} = :gen_tcp.listen(port, options)
    {:ok, socket} = :gen_tcp.accept(listen_socket) # TODO 不会阻塞在这里么?
    {:ok, %{ip: ip, port: port, socket: socket}} # 保存的是连进来的这个客户端
  end

  def handle_info({:tcp, socket, packet}, state) do
    Logger.info "receive packet, length: #{byte_size packet}."
    IO.inspect packet, label: "receiving packet"
    :gen_tcp.send socket, "HTTP/1.1 200\r\nContent-Length: 0\r\n\r\n"
    {:noreply, state}
  end

  def handle_info({:tcp_closed, socket}, state) do
    Logger.info "tcp connection closed normly."
    {:noreply, state}
  end

  def handle_info({:tcp_error, socket, reason}, state) do
    Logger.info "tcp connecton closed because #{reason}."
    {:noreply, state}
  end
end
