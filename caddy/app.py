import time
import logging
from flask import Flask, jsonify, request

app = Flask(__name__)
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(message)s", filename="logs/app.log"
)
log = logging.getLogger()


@app.route("/")
def hello():
    return jsonify({"message": "Hello from Flask!", "status": "running"})


@app.route("/sleep")
def health():
    log.info("args: %s", request.args)
    sleep_ms = 0  # int(request.args.get("ms", "0"))
    sleep_sec = int(request.args.get("sec", "0"))
    selected_sleep = sleep_sec if sleep_sec > 0 else sleep_ms / 1000

    app.logger.info("sleep for %s s", selected_sleep)
    time.sleep(selected_sleep)

    return jsonify({"status": "healthy"})


if __name__ == "__main__":
    app.run()
