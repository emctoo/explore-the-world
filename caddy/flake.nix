{
  description = "A flake with xcaddy, caddy and Go toolchain supporting cross-compilation";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    pyproject-nix.url = "github:pyproject-nix/pyproject.nix";
    pyproject-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, pyproject-nix, ... }:
    let
      supportedSystems = [ "x86_64-linux" "aarch64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      pkgsFor = system: import nixpkgs {
        inherit system;
        crossSystem = if system == "x86_64-linux" then { config = "aarch64-unknown-linux-gnu"; } else null;
      };

    in {
      devShells = forAllSystems (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          targetPkgs = if system == "x86_64-linux" then pkgsFor "aarch64-linux" else pkgs;
          pythonEnv = pkgs.python3.withPackages (ps: with ps; [ flask ]);
        in {
          default = pkgs.mkShell {
            buildInputs = [
              pkgs.caddy
              pkgs.xcaddy
              pkgs.just
              pythonEnv

              # Go 工具链
              pkgs.go_1_22
              pkgs.gotools
              pkgs.gopls
              pkgs.go-outline
              pkgs.gocode-gomod
              pkgs.golint
              pkgs.delve

              # 交叉编译工具
              pkgs.buildPackages.binutils
            ];

            # 设置环境变量
            shellHook = ''
              export GOPROXY=https://goproxy.cn,direct
              export GOPATH=$HOME/go
              export PATH=$GOPATH/bin:$PATH

              echo "Go development environment ready!"
              just --list
            '';
          };
        }
      );
    };
}
