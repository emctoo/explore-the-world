#!/usr/bin/env python
# coding: utf-8

import argparse
import sys
import os
import re
import urllib
import json
import socket

import urllib.request
import urllib.parse
import urllib.error

import time
import logging
import hashlib

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')
logger = logging.getLogger()

timeout = 5
socket.setdefaulttimeout(timeout)


class Crawler:
  # 睡眠时长
  __time_sleep = 0.1
  __amount = 0
  __start_amount = 0
  __counter = 0
  headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}

  def __init__(self, interval=0.05):
    self.time_sleep = interval


  def get_suffix(self, name):
    # 获取后缀名
    m = re.search(r'\.[^\.]*$', name)
    if m.group(0) and len(m.group(0)) <= 5:
      return m.group(0)
    else:
      return '.jpeg'

  # 获取referrer，用于生成referrer
  def get_referrer(self, url):
    par = urllib.parse.urlparse(url)
    if par.scheme:
      return par.scheme + '://' + par.netloc
    else:
      return par.netloc

  def save_image(self, rsp_data, word):
    # 保存图片， 关键词命名目录
    output_dir = self.output_dir

    if not os.path.exists(output_dir):
      logger.info('create dir %s', output_dir)
      os.mkdir(output_dir)

    # 判断名字是否重复，获取图片长度
    self.__counter = len(os.listdir(output_dir)) + 1
    for image_info in rsp_data['imgs']:
      # logger.info('retreive image %s', image_info)

      try:
        time.sleep(self.time_sleep)

        suffix = self.get_suffix(image_info['objURL'])

        # 指定UA和referrer，减少403
        refer = self.get_referrer(image_info['objURL'])
        opener = urllib.request.build_opener()
        opener.addheaders = [
          ('User-agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0'),
          ('Referer', refer),
        ]

        urllib.request.install_opener(opener)

        # urllib.request.urlretrieve(image_info['objURL'], output_image_path)
        with urllib.request.urlopen(image_info['objURL']) as f:
          buf = f.read()
          content_hash = hashlib.md5(buf).hexdigest()
          output_image_path = os.path.join(output_dir, f'{content_hash}{suffix}')

          logger.info('retreive image %s to %s', image_info['objURL'], output_image_path)
          if os.path.exists(output_image_path):
            logger.info('file %s exists, will not save', output_image_path)
            return

          with open(output_image_path, 'wb') as output_image_handle:
            output_image_handle.write(buf)

      except urllib.error.HTTPError as urllib_err:
        logger.info('http error: %s', urllib_err)
        continue

      except Exception as err:
        time.sleep(1)
        logger.info(err)
        logger.info("产生未知错误，放弃保存")
        continue

      else:
        self.__counter += 1

  # 开始获取
  def get_images(self, word='美女'):
    search = urllib.parse.quote(word)
    # pn int 图片数
    pn = self.__start_amount

    while pn < self.__amount:
      host = 'http://image.baidu.com/search/avatarjson'
      url = f'{host}?tn=resultjsonavatarnew&ie=utf-8&word={search}&cg=girl&pn={pn}&rn=60&itg=0&z=2&fr=&width=&height=&lm=-1&ic=0&s=0&st=-1&gsm=1e0000001e'

      # 设置header防ban
      try:
        time.sleep(self.time_sleep)
        req = urllib.request.Request(url=url, headers=self.headers)
        page = urllib.request.urlopen(req)
        rsp = page.read().decode('unicode_escape')

      except UnicodeDecodeError as e:
        logger.info('UnicodeDecodeErrorurl, url: %s, error: %s', url, e)

      except urllib.error.URLError as e:
        logger.info("urlErrorurl, url: %s, error: %s", url, e)

      except socket.timeout as e:
        logger.info("socket timout, url: %s, error: %s", url, e)

      else:
        # 解析json
        rsp_data = json.loads(rsp)
        self.save_image(rsp_data, word)

        # 读取下一页
        logger.info("下载下一页")
        pn += 60

      finally:
        page.close()

    logger.info("下载任务结束")

  def start(self, keyword, page_count=1, start_page_index=1, output_dir=None):
    """
    :param keyword: 抓取的关键词
    :param page_count: 需要抓取数据页数 总抓取图片数量为 页数x60
    :param start_page_index: 起始页数
    :return:
    """
    self.output_dir = os.path.join('/tmp', keyword) if output_dir is None else output_dir

    self.__start_amount = (start_page_index - 1) * 60
    self.__amount = page_count * 60 + self.__start_amount

    self.get_images(keyword)


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='download image by baidu')
  parser.add_argument('keyword', type=str, help='image search keyword')
  parser.add_argument('-o', dest='output_dir', type=str, default=None, help='output dir')
  parser.add_argument('-c', dest='page_count', type=int, default=1, help='page count to download')
  parser.add_argument('-i', dest='start_page_index', type=int, default=1, help='starting page index to download')
  parser.add_argument('--interval', dest='interval', type=float, default=0.05, help='download interval, default 50ms')

  args = parser.parse_args()
  logger.info('args: %s', args.keyword)

  if args.output_dir:
    args.output_dir = os.path.abspath(args.output_dir)
    logger.info('output_dir: %s', args.output_dir)

  crawler = Crawler(interval=args.interval)  # 抓取延迟为 0.05
  crawler.start(args.keyword,
    page_count=args.page_count,
    start_page_index=args.start_page_index,
    output_dir=args.output_dir,
  )
